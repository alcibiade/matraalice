package org.alcibiade.emulation.mc6847;

import org.alcibiade.emulation.core.memory.VoidMemory;
import org.junit.Test;

public class MC6847Test {

    @Test
    public void testGPU() {
        MC6847 gpu = new MC6847();
        gpu.setMemory(new VoidMemory());

        gpu.getDebugInformations();
        gpu.getDisplayResolution();
    }
}
