package org.alcibiade.emulation.mc6847;

import org.alcibiade.emulation.core.video.VdgFont;
import org.junit.Assert;
import org.junit.Test;

public class MC6847FontTest {

    @Test
    public void testFont() {
        VdgFont font = new MC6847Font();
        byte[] shape = font.getCharacterShape(12);
        Assert.assertEquals(12, shape.length);
    }
}
