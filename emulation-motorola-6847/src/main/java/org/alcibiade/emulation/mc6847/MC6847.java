package org.alcibiade.emulation.mc6847;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryUtils;
import org.alcibiade.emulation.core.video.AbstractGpu;
import org.alcibiade.emulation.core.video.VdgFont;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MC6847 extends AbstractGpu {

    private Logger logger = LoggerFactory.getLogger(MC6847.class);
    private MC6847DisplayMode displayMode = MC6847DisplayMode.S32;
    private VdgFont font = new MC6847Font();

    @Override
    public Dimension getDisplayResolution() {
        return new Dimension(34 * 8, 18 * 12);
    }

    @Override
    public String getDebugInformations() {
        StringBuilder text = new StringBuilder();
        text.append("=== MC6847 ==========================================================\n");
        text.append(MemoryUtils.dump(getMemory(), 0, 512));
        text.append("=====================================================================\n");
        return text.toString();
    }

    @Override
    public void paint(Graphics2D g2) {
        Dimension displaySize = this.getDisplayResolution();

        g2.setColor(Color.BLACK);
        g2.fillRect(0, 0, displaySize.width, displaySize.height);

        int offsetX = (34 - displayMode.getColumns()) * 8 / 2;
        int offsetY = (18 - displayMode.getRows()) * 12 / 2;

        g2.translate(offsetX, offsetY);

        try {
            for (int y = 0; y < displayMode.getRows(); y++) {
                for (int x = 0; x < displayMode.getColumns(); x++) {
                    int code = getMemory().getByte(y * displayMode.getColumns() + x);
                    byte[] shape = font.getCharacterShape(code);
                    drawShape(g2, x * 8, y * 12, shape);
                }
            }
        } catch (IllegalMemoryAccessException ex) {
            logger.error(ex.getLocalizedMessage(), ex);
        }

    }

    private void drawShape(Graphics2D g2, int x, int y, byte[] shape) {
        for (int dy = 0; dy < 12; dy++) {
            byte line = shape[dy];

            for (int dx = 0; dx < 8; dx++) {
                int mask = (1 << (7 - dx));
                int value = mask & line;

                g2.setColor((value == 0) ? Color.GREEN : Color.BLACK);

                g2.fillRect(x + dx, y + dy, 1, 1);
            }
        }
    }
}
