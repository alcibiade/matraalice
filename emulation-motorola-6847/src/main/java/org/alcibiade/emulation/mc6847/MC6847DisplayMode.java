package org.alcibiade.emulation.mc6847;

public enum MC6847DisplayMode {

    S32(32, 16, 8);
    private int columns;
    private int rows;
    private int colors;

    private MC6847DisplayMode(int columns, int rows, int colors) {
        this.columns = columns;
        this.rows = rows;
        this.colors = colors;
    }

    public int getColumns() {
        return columns;
    }

    public int getRows() {
        return rows;
    }

    public int getColors() {
        return colors;
    }

    @Override
    public String toString() {
        return String.format("%s{%dx%d, %dcol}", columns, rows, colors);
    }
}
