package org.alcibiade.emulation.alice;

public class KeyMatrixPosition {

    private int register;
    private int bit;

    public KeyMatrixPosition(int register, int bit) {
        assert register >= 0;
        assert register < 8;
        assert bit >= 0;
        assert bit < 8;

        this.register = register;
        this.bit = bit;
    }

    public int getRegister() {
        return register;
    }

    public int getBit() {
        return bit;
    }

    public int getBitMask() {
        return 1 << bit;
    }
}
