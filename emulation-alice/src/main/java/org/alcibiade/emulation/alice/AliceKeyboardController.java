package org.alcibiade.emulation.alice;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.alcibiade.emulation.core.keyboard.AbstractKeyboardController;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryCell;
import org.alcibiade.emulation.core.memory.MemoryCellListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

public class AliceKeyboardController extends AbstractKeyboardController
        implements MemoryCellListener {

    private static final byte SPECIAL_KEY_PRESSED = 77;
    private static final byte SPECIAL_KEY_NO = 79;
    private Logger logger = LoggerFactory.getLogger(AliceKeyboardController.class);
    private MemoryCell keyboardControlCell;
    private MemoryCell keyboardLinesCell;
    private MemoryCell keyboardSpecialCell;
    private byte[] statusMatrix = new byte[8];
    private boolean statusShift = false;
    private boolean statusBreak = false;
    private boolean statusControl = false;
    private Map<Integer, KeyMatrixPosition> keyMap;

    @Required
    public void setKeyboardControlCell(MemoryCell keyboardControlCell) {
        this.keyboardControlCell = keyboardControlCell;
    }

    @Required
    public void setKeyboardOutputLinesCell(MemoryCell keyboardOutputLinesCell) {
        this.keyboardLinesCell = keyboardOutputLinesCell;
    }

    @Required
    public void setKeyboardSpecialCell(MemoryCell keyboardSpecialCell) {
        this.keyboardSpecialCell = keyboardSpecialCell;
    }

    // CHECKSTYLE IGNORE ExecutableStatementCountCheck FOR NEXT 4 LINES
    @PostConstruct
    public void init() throws IllegalMemoryAccessException {
        this.keyboardLinesCell.addMemoryCellListener(this);

        for (int i = 0; i < 8; i++) {
            this.statusMatrix[i] = (byte) 0xFF;
        }

        keyMap = new HashMap<Integer, KeyMatrixPosition>();
        keyMap.put(KeyEvent.VK_A, new KeyMatrixPosition(1, 0));
        keyMap.put(KeyEvent.VK_B, new KeyMatrixPosition(2, 0));
        keyMap.put(KeyEvent.VK_C, new KeyMatrixPosition(3, 0));
        keyMap.put(KeyEvent.VK_D, new KeyMatrixPosition(4, 0));
        keyMap.put(KeyEvent.VK_E, new KeyMatrixPosition(5, 0));
        keyMap.put(KeyEvent.VK_F, new KeyMatrixPosition(6, 0));
        keyMap.put(KeyEvent.VK_G, new KeyMatrixPosition(7, 0));
        keyMap.put(KeyEvent.VK_H, new KeyMatrixPosition(0, 1));
        keyMap.put(KeyEvent.VK_I, new KeyMatrixPosition(1, 1));
        keyMap.put(KeyEvent.VK_J, new KeyMatrixPosition(2, 1));
        keyMap.put(KeyEvent.VK_K, new KeyMatrixPosition(3, 1));
        keyMap.put(KeyEvent.VK_L, new KeyMatrixPosition(4, 1));
        keyMap.put(KeyEvent.VK_M, new KeyMatrixPosition(5, 1));
        keyMap.put(KeyEvent.VK_N, new KeyMatrixPosition(6, 1));
        keyMap.put(KeyEvent.VK_O, new KeyMatrixPosition(7, 1));
        keyMap.put(KeyEvent.VK_P, new KeyMatrixPosition(0, 2));
        keyMap.put(KeyEvent.VK_Q, new KeyMatrixPosition(1, 2));
        keyMap.put(KeyEvent.VK_R, new KeyMatrixPosition(2, 2));
        keyMap.put(KeyEvent.VK_S, new KeyMatrixPosition(3, 2));
        keyMap.put(KeyEvent.VK_T, new KeyMatrixPosition(4, 2));
        keyMap.put(KeyEvent.VK_U, new KeyMatrixPosition(5, 2));
        keyMap.put(KeyEvent.VK_V, new KeyMatrixPosition(6, 2));
        keyMap.put(KeyEvent.VK_W, new KeyMatrixPosition(7, 2));
        keyMap.put(KeyEvent.VK_X, new KeyMatrixPosition(0, 3));
        keyMap.put(KeyEvent.VK_Y, new KeyMatrixPosition(1, 3));
        keyMap.put(KeyEvent.VK_Z, new KeyMatrixPosition(2, 3));
        keyMap.put(KeyEvent.VK_ENTER, new KeyMatrixPosition(6, 3));
        keyMap.put(KeyEvent.VK_SPACE, new KeyMatrixPosition(7, 3));
        keyMap.put(KeyEvent.VK_0, new KeyMatrixPosition(0, 4));
        keyMap.put(KeyEvent.VK_1, new KeyMatrixPosition(1, 4));
        keyMap.put(KeyEvent.VK_2, new KeyMatrixPosition(2, 4));
        keyMap.put(KeyEvent.VK_3, new KeyMatrixPosition(3, 4));
        keyMap.put(KeyEvent.VK_4, new KeyMatrixPosition(4, 4));
        keyMap.put(KeyEvent.VK_5, new KeyMatrixPosition(5, 4));
        keyMap.put(KeyEvent.VK_6, new KeyMatrixPosition(6, 4));
        keyMap.put(KeyEvent.VK_7, new KeyMatrixPosition(7, 4));
        keyMap.put(KeyEvent.VK_8, new KeyMatrixPosition(0, 5));
        keyMap.put(KeyEvent.VK_9, new KeyMatrixPosition(1, 5));
        keyMap.put(KeyEvent.VK_COLON, new KeyMatrixPosition(2, 5));
        keyMap.put(KeyEvent.VK_SEMICOLON, new KeyMatrixPosition(3, 5));
        keyMap.put(KeyEvent.VK_PAUSE, new KeyMatrixPosition(4, 5));
        keyMap.put(KeyEvent.VK_MINUS, new KeyMatrixPosition(5, 5));
        keyMap.put(KeyEvent.VK_PERIOD, new KeyMatrixPosition(6, 5));
        keyMap.put(KeyEvent.VK_SLASH, new KeyMatrixPosition(7, 5));
    }

    @Override
    public void processKeyPress(int keyCode) throws IllegalMemoryAccessException {
        this.logger.debug("Key pressed: " + keyCode);
        updateMatrix(keyCode, true);
    }

    @Override
    public void processKeyRelease(int keyCode) throws IllegalMemoryAccessException {
        this.logger.debug("Key released: " + keyCode);
        updateMatrix(keyCode, false);
    }

    @Override
    public void notifyMemoryCellUpdated(byte newValue) throws IllegalMemoryAccessException {
        byte keyRegister = (byte) 0xFF;

        for (int i = 0; i < 8; i++) {
            if ((newValue & (1 << i)) == 0) {
                keyRegister &= statusMatrix[i];
            }
        }

        if (keyRegister != (byte) 0xFF) {
            logger.debug(String.format("[KEY] %02X -> %02X", newValue, keyRegister));
        }

        this.keyboardControlCell.setByte(0, keyRegister);

        // Special keys Shift/Ctrl/Brk management
        byte specialRegister = SPECIAL_KEY_NO;

        if ((newValue & 0x80) == 0 && statusShift) {
            specialRegister = SPECIAL_KEY_PRESSED;
        } else if ((newValue & 0x01) == 0 && statusControl) {
            specialRegister = SPECIAL_KEY_PRESSED;
        } else if ((newValue & 0x04) == 0 && statusBreak) {
            specialRegister = SPECIAL_KEY_PRESSED;
        }

        this.keyboardSpecialCell.setByte(0, specialRegister);
    }

    private void updateMatrix(int keyCode, boolean keyPressed) {
        if (keyCode == KeyEvent.VK_SHIFT) {
            statusShift = keyPressed;
        } else if (keyCode == KeyEvent.VK_CONTROL) {
            statusControl = keyPressed;
        } else if (keyCode == KeyEvent.VK_ESCAPE) {
            statusBreak = keyPressed;
        } else {
            KeyMatrixPosition position = keyMap.get(keyCode);

            if (position != null) {
                if (keyPressed) {
                    this.statusMatrix[position.getRegister()] &= 0xFF - position.getBitMask();
                } else {
                    this.statusMatrix[position.getRegister()] |= position.getBitMask();
                }
            }
        }
    }
}
