package org.alcibiade.emulation.alice;

import java.util.Timer;
import java.util.TimerTask;
import javax.annotation.PostConstruct;
import org.alcibiade.emulation.core.memory.MemoryUtils;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

public class AliceDebugger extends TimerTask {

    private static final long REFRESH_INTERVAL_MILLIS = 3000;
    private RandomAccessible memory;
    private Logger logger = LoggerFactory.getLogger(AliceDebugger.class);

    @Required
    public void setMemory(RandomAccessible memory) {
        this.memory = memory;
    }

    @PostConstruct
    public void init() {
        Timer refreshTimer = new Timer();
        refreshTimer.schedule(this, REFRESH_INTERVAL_MILLIS, REFRESH_INTERVAL_MILLIS);
    }

    @Override
    public void run() {
        logger.info("=== DEBUGGER =====================================");
        logger.info("--- Keyboard status ------------------------------\n"
                + MemoryUtils.dump(memory, 0x4230, 16));
        logger.info("--- Cursor input--- ------------------------------\n"
                + MemoryUtils.dump(memory, 0x427F, 4));
        logger.info("--- Command line buffer --------------------------\n"
                + MemoryUtils.dump(memory, 0x42B2, 128));
        logger.info("==================================================");
        logger.info("--");
    }
}
