package org.alcibiade.emulation.alice;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AliceEmulator {

    public static void main(String... args) {
        new ClassPathXmlApplicationContext("org/alcibiade/emulation/alice/context-alice4k.xml");
    }
}
