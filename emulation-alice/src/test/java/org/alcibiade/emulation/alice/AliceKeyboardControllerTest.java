package org.alcibiade.emulation.alice;

import java.awt.event.KeyEvent;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryCell;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class AliceKeyboardControllerTest {

    @Test
    public void testAllLetters() throws IllegalMemoryAccessException {
        MemoryCell keyboardControlCell = new MemoryCell();
        MemoryCell keyboardOutputLinesCell = new MemoryCell();
        MemoryCell keyboardSpecialCell = new MemoryCell();

        AliceKeyboardController keyboardController = new AliceKeyboardController();
        keyboardController.setKeyboardControlCell(keyboardControlCell);
        keyboardController.setKeyboardOutputLinesCell(keyboardOutputLinesCell);
        keyboardController.setKeyboardSpecialCell(keyboardSpecialCell);
        keyboardController.init();

        keyboardController.processKeyPress(KeyEvent.VK_Y);
        keyboardOutputLinesCell.setByte(0, (byte) 253);
        assertEquals((byte) 0xF7, keyboardControlCell.getByte(0));
        keyboardController.processKeyRelease(KeyEvent.VK_Y);

        keyboardController.processKeyPress(KeyEvent.VK_G);
        keyboardOutputLinesCell.setByte(0, (byte) 127);
        assertEquals((byte) 0xFE, keyboardControlCell.getByte(0));
        keyboardController.processKeyRelease(KeyEvent.VK_G);
    }

    @Test
    public void testY() throws IllegalMemoryAccessException {
        MemoryCell keyboardControlCell = new MemoryCell();
        MemoryCell keyboardOutputLinesCell = new MemoryCell();
        MemoryCell keyboardSpecialCell = new MemoryCell();

        AliceKeyboardController keyboardController = new AliceKeyboardController();
        keyboardController.setKeyboardControlCell(keyboardControlCell);
        keyboardController.setKeyboardOutputLinesCell(keyboardOutputLinesCell);
        keyboardController.setKeyboardSpecialCell(keyboardSpecialCell);
        keyboardController.init();

        // Columns without any key pressed
        for (int i = 0; i < 8; i++) {
            keyboardOutputLinesCell.setByte(0, (byte) (0xFF - (1 << i)));
            assertEquals((byte) 0xFF, keyboardControlCell.getByte(0));
            assertEquals((byte) 79, keyboardSpecialCell.getByte(0));
        }

        keyboardController.processKeyPress(KeyEvent.VK_Y);

        keyboardOutputLinesCell.setByte(0, (byte) 127);
        assertEquals((byte) 0xFF, keyboardControlCell.getByte(0));
        keyboardOutputLinesCell.setByte(0, (byte) 191);
        assertEquals((byte) 0xFF, keyboardControlCell.getByte(0));
        keyboardOutputLinesCell.setByte(0, (byte) 223);
        assertEquals((byte) 0xFF, keyboardControlCell.getByte(0));
        keyboardOutputLinesCell.setByte(0, (byte) 239);
        assertEquals((byte) 0xFF, keyboardControlCell.getByte(0));
        keyboardOutputLinesCell.setByte(0, (byte) 247);
        assertEquals((byte) 0xFF, keyboardControlCell.getByte(0));
        keyboardOutputLinesCell.setByte(0, (byte) 251);
        assertEquals((byte) 0xFF, keyboardControlCell.getByte(0));
        keyboardOutputLinesCell.setByte(0, (byte) 253);
        assertEquals((byte) 0xF7, keyboardControlCell.getByte(0));
        keyboardOutputLinesCell.setByte(0, (byte) 254);
        assertEquals((byte) 0xFF, keyboardControlCell.getByte(0));

        keyboardController.processKeyRelease(KeyEvent.VK_Y);

        // Columns without any key pressed
        for (int i = 0; i < 8; i++) {
            keyboardOutputLinesCell.setByte(0, (byte) (0xFF - (1 << i)));
            assertEquals((byte) 0xFF, keyboardControlCell.getByte(0));
            assertEquals((byte) 79, keyboardSpecialCell.getByte(0));
        }

    }

    @Test
    public void testShift() throws IllegalMemoryAccessException {
        MemoryCell keyboardControlCell = new MemoryCell();
        MemoryCell keyboardOutputLinesCell = new MemoryCell();
        MemoryCell keyboardSpecialCell = new MemoryCell();

        AliceKeyboardController keyboardController = new AliceKeyboardController();
        keyboardController.setKeyboardControlCell(keyboardControlCell);
        keyboardController.setKeyboardOutputLinesCell(keyboardOutputLinesCell);
        keyboardController.setKeyboardSpecialCell(keyboardSpecialCell);
        keyboardController.init();

        keyboardOutputLinesCell.setByte(0, (byte) 127);
        assertEquals((byte) 79, keyboardSpecialCell.getByte(0));
        keyboardOutputLinesCell.setByte(0, (byte) 251);
        assertEquals((byte) 79, keyboardSpecialCell.getByte(0));
        keyboardOutputLinesCell.setByte(0, (byte) 254);
        assertEquals((byte) 79, keyboardSpecialCell.getByte(0));

        keyboardController.processKeyPress(KeyEvent.VK_SHIFT);

        keyboardOutputLinesCell.setByte(0, (byte) 127);
        assertEquals((byte) 77, keyboardSpecialCell.getByte(0));
        keyboardOutputLinesCell.setByte(0, (byte) 251);
        assertEquals((byte) 79, keyboardSpecialCell.getByte(0));
        keyboardOutputLinesCell.setByte(0, (byte) 254);
        assertEquals((byte) 79, keyboardSpecialCell.getByte(0));

        keyboardController.processKeyRelease(KeyEvent.VK_SHIFT);

        keyboardOutputLinesCell.setByte(0, (byte) 127);
        assertEquals((byte) 79, keyboardSpecialCell.getByte(0));
        keyboardOutputLinesCell.setByte(0, (byte) 251);
        assertEquals((byte) 79, keyboardSpecialCell.getByte(0));
        keyboardOutputLinesCell.setByte(0, (byte) 254);
        assertEquals((byte) 79, keyboardSpecialCell.getByte(0));

    }
}
