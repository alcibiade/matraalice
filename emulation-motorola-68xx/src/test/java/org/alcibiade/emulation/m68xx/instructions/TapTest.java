package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.junit.Assert;
import org.junit.Test;

public class TapTest {

    @Test
    public void testCopy() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register a = new StaticRegister("A", 8);

        Instruction inst = new Tap(pc, a, ccr);
        Assert.assertEquals(2, inst.getCycles());
        Assert.assertEquals(1, inst.getSize());

        a.setValue(5);

        inst.execute();
        Assert.assertEquals(5, a.getSignedValue());
        Assert.assertEquals(false, ccr.getI());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getC());
    }
}
