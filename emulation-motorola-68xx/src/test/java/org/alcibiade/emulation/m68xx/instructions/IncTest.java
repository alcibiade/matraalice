package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Assert;
import org.junit.Test;

public class IncTest {

    @Test
    public void testINCReg() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register a = new StaticRegister("A", 8);
        Instruction inc = new Inc(pc, a, ccr);
        Assert.assertEquals(2, inc.getCycles());
        Assert.assertEquals(1, inc.getSize());

        ccr.setC(true);
        a.setValue(-4);
        inc.execute();
        Assert.assertEquals(-3, a.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());

        ccr.setC(false);
        a.setValue(-4);
        inc.execute();
        Assert.assertEquals(-3, a.getSignedValue());
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());

        a.setValue(-1);
        inc.execute();
        Assert.assertEquals(0, a.getSignedValue());
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getZ());

        a.setValue(127);
        inc.execute();
        Assert.assertEquals(-128, a.getSignedValue());
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());
    }

    @Test
    public void testINCMem() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 8);
        CCR ccr = new CCR();
        RandomAccessible mem = new MemoryArea(12);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        MemoryAccessor memoryAccessor = new MemoryAccessor(memMgr, MemoryAccessMode.EXTENDED, pc, pc,
                1);
        Instruction inc = new Inc(pc, memoryAccessor, ccr);
        Assert.assertEquals(6, inc.getCycles());
        Assert.assertEquals(3, inc.getSize());

        ccr.setC(false);
        mem.setByte(0, (byte) -4);
        inc.execute();
        Assert.assertEquals(-3, mem.getByte(0));
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());

        mem.setByte(0, (byte) -1);
        inc.execute();
        Assert.assertEquals(0, mem.getByte(0));
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
    }
}
