package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Assert;
import org.junit.Test;

public class CmpTest {

    @Test
    public void testBasicComparison() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        Register a = new StaticRegister("A", 8);
        RandomAccessible mem = new MemoryArea(1024);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        MemoryAccessor memoryAccessor = new MemoryAccessor(memMgr, MemoryAccessMode.IMMEDIATE, pc,
                pc,
                1);

        // Operands
        a.setValue(0xD0);
        mem.setByte(1, (byte) 0x20);

        CCR ccr = new CCR();
        ccr.setC(true);
        ccr.setN(true);
        ccr.setV(true);
        ccr.setZ(false);

        Instruction cmp = new Cmp(pc, a, memoryAccessor, ccr);
        Assert.assertEquals(2, cmp.getCycles());
        Assert.assertEquals(2, cmp.getSize());
        cmp.execute();

        Assert.assertEquals((byte)0xD0, a.getSignedValue());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(false, ccr.getV());
    }

}
