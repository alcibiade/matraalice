package org.alcibiade.emulation.m68xx.instructions;

import org.junit.Assert;
import org.junit.Test;

public class CCRTest {

    @Test
    public void testCCRToString() {
        CCR ccr = new CCR();
        Assert.assertNotNull(ccr.toString());
    }

    @Test
    public void testCCRUpdates() {
        CCR ccr = new CCR();
        Assert.assertFalse(ccr.getH());
        Assert.assertFalse(ccr.getC());
        Assert.assertFalse(ccr.getV());
        Assert.assertFalse(ccr.getN());
        Assert.assertFalse(ccr.getZ());
        Assert.assertFalse(ccr.getI());

        ccr.setC(true);
        Assert.assertFalse(ccr.getH());
        Assert.assertTrue(ccr.getC());
        Assert.assertFalse(ccr.getV());
        Assert.assertFalse(ccr.getN());
        Assert.assertFalse(ccr.getZ());
        Assert.assertFalse(ccr.getI());

        ccr.setC(false);
        Assert.assertFalse(ccr.getH());
        Assert.assertFalse(ccr.getC());
        Assert.assertFalse(ccr.getV());
        Assert.assertFalse(ccr.getN());
        Assert.assertFalse(ccr.getZ());
        Assert.assertFalse(ccr.getI());

        ccr.setV(true);
        Assert.assertFalse(ccr.getH());
        Assert.assertFalse(ccr.getC());
        Assert.assertTrue(ccr.getV());
        Assert.assertFalse(ccr.getN());
        Assert.assertFalse(ccr.getZ());
        Assert.assertFalse(ccr.getI());

        ccr.setV(false);
        Assert.assertFalse(ccr.getH());
        Assert.assertFalse(ccr.getC());
        Assert.assertFalse(ccr.getV());
        Assert.assertFalse(ccr.getN());
        Assert.assertFalse(ccr.getZ());
        Assert.assertFalse(ccr.getI());

        ccr.setN(true);
        Assert.assertFalse(ccr.getH());
        Assert.assertFalse(ccr.getC());
        Assert.assertFalse(ccr.getV());
        Assert.assertTrue(ccr.getN());
        Assert.assertFalse(ccr.getZ());
        Assert.assertFalse(ccr.getI());

        ccr.setN(false);
        Assert.assertFalse(ccr.getH());
        Assert.assertFalse(ccr.getC());
        Assert.assertFalse(ccr.getV());
        Assert.assertFalse(ccr.getN());
        Assert.assertFalse(ccr.getZ());
        Assert.assertFalse(ccr.getI());

        ccr.setZ(true);
        Assert.assertFalse(ccr.getH());
        Assert.assertFalse(ccr.getC());
        Assert.assertFalse(ccr.getV());
        Assert.assertFalse(ccr.getN());
        Assert.assertTrue(ccr.getZ());
        Assert.assertFalse(ccr.getI());

        ccr.setZ(false);
        Assert.assertFalse(ccr.getH());
        Assert.assertFalse(ccr.getC());
        Assert.assertFalse(ccr.getV());
        Assert.assertFalse(ccr.getN());
        Assert.assertFalse(ccr.getZ());
        Assert.assertFalse(ccr.getI());

        ccr.setI(true);
        Assert.assertFalse(ccr.getH());
        Assert.assertFalse(ccr.getC());
        Assert.assertFalse(ccr.getV());
        Assert.assertFalse(ccr.getN());
        Assert.assertFalse(ccr.getZ());
        Assert.assertTrue(ccr.getI());

        ccr.setI(false);
        Assert.assertFalse(ccr.getH());
        Assert.assertFalse(ccr.getC());
        Assert.assertFalse(ccr.getV());
        Assert.assertFalse(ccr.getN());
        Assert.assertFalse(ccr.getZ());
        Assert.assertFalse(ccr.getI());

        ccr.setH(true);
        Assert.assertTrue(ccr.getH());
        Assert.assertFalse(ccr.getC());
        Assert.assertFalse(ccr.getV());
        Assert.assertFalse(ccr.getN());
        Assert.assertFalse(ccr.getZ());
        Assert.assertFalse(ccr.getI());

        ccr.setH(false);
        Assert.assertFalse(ccr.getH());
        Assert.assertFalse(ccr.getC());
        Assert.assertFalse(ccr.getV());
        Assert.assertFalse(ccr.getN());
        Assert.assertFalse(ccr.getZ());
        Assert.assertFalse(ccr.getI());
    }
}
