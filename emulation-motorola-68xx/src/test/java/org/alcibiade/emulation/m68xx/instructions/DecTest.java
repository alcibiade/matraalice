package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Assert;
import org.junit.Test;

public class DecTest {

    @Test
    public void testDECReg() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register a = new StaticRegister("A", 8);
        Instruction dec = new Dec(pc, a, ccr);
        Assert.assertEquals(2, dec.getCycles());
        Assert.assertEquals(1, dec.getSize());

        ccr.setC(true);
        a.setValue(-4);
        dec.execute();
        Assert.assertEquals(-5, a.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getN());

        ccr.setC(false);
        a.setValue(-4);
        dec.execute();
        Assert.assertEquals(-5, a.getSignedValue());
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getN());

        a.setValue(1);
        dec.execute();
        Assert.assertEquals(0, a.getSignedValue());
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getN());

        a.setValue(0x80);
        dec.execute();
        Assert.assertEquals(127, a.getSignedValue());
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getN());
    }

    @Test
    public void testDECMem() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 8);
        CCR ccr = new CCR();
        RandomAccessible mem = new MemoryArea(12);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        Instruction dec = new Dec(pc,
                new MemoryAccessor(memMgr, MemoryAccessMode.EXTENDED, pc, pc, 1), ccr);
        Assert.assertEquals(6, dec.getCycles());
        Assert.assertEquals(3, dec.getSize());

        ccr.setC(false);
        mem.setByte(0, (byte) -4);
        dec.execute();
        Assert.assertEquals(-5, mem.getByte(0));
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());
    }
}
