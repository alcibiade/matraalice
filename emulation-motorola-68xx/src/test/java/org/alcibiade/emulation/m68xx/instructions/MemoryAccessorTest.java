package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class MemoryAccessorTest {

    @Test
    public void testDirectWordMemoryAccess() throws IllegalMemoryAccessException {
        RandomAccessible memory = new MemoryArea(1024);
        MemoryManager manager = new BigEndianMemoryManager(memory);

        Register regPC = new StaticRegister("PC", 16);
        MemoryAccessor accessor = new MemoryAccessor(manager, MemoryAccessMode.DIRECT, regPC, null,
                2);

        memory.setByte(1, (byte) 0x10);
        assertEquals(0x10, accessor.getAddress());

        accessor.setValue(0xABCD);

        assertEquals((byte) 0xAB, memory.getByte(0x10));
        assertEquals((byte) 0xCD, memory.getByte(0x11));

        int value = accessor.getSignedValue();
        assertEquals((short) 0xABCD, value);
    }

    @Test
    public void testDirectByteMemoryAccess() throws IllegalMemoryAccessException {
        RandomAccessible memory = new MemoryArea(1024);
        MemoryManager manager = new BigEndianMemoryManager(memory);

        Register regPC = new StaticRegister("PC", 16);
        MemoryAccessor accessor = new MemoryAccessor(manager, MemoryAccessMode.DIRECT, regPC, null,
                1);

        memory.setByte(1, (byte) 0x10);
        accessor.setValue(0xAB);

        assertEquals((byte) 0xAB, memory.getByte(0x10));

        int value = accessor.getSignedValue();
        assertEquals((byte) 0xAB, value);
    }
}
