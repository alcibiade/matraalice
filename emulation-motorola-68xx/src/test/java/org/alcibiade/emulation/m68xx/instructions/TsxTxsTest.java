package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.junit.Assert;
import org.junit.Test;

public class TsxTxsTest {

    @Test
    public void testOperation() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register x = new StaticRegister("X", 16);
        Register s = new StaticRegister("SP", 16);


        x.setValue(0x3F00);
        s.setValue(0x1F00);

        Instruction tsx = new Tsx(pc, x, s, ccr);
        Assert.assertEquals(4, tsx.getCycles());
        Assert.assertEquals(1, tsx.getSize());

        tsx.execute();

        Assert.assertEquals(0x1F01, x.getUnsignedValue());
        Assert.assertEquals(0x1F00, s.getUnsignedValue());
        Assert.assertEquals(false, ccr.getI());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getC());

        s.setValue(0x3333);

        Instruction txs = new Txs(pc, x, s, ccr);
        Assert.assertEquals(4, txs.getCycles());
        Assert.assertEquals(1, txs.getSize());

        txs.execute();

        Assert.assertEquals(0x1F01, x.getUnsignedValue());
        Assert.assertEquals(0x1F00, s.getUnsignedValue());
        Assert.assertEquals(false, ccr.getI());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getC());
    }
}
