package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.junit.Assert;
import org.junit.Test;

public class SetFlagTest {

    @Test
    public void testSetCarry() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        ccr.setC(false);
        ccr.setN(true);
        ccr.setV(true);
        ccr.setZ(false);

        Instruction setC = new SetCarry(pc, ccr, true);
        Assert.assertEquals(2, setC.getCycles());
        Assert.assertEquals(1, setC.getSize());
        setC.execute();

        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());

        Instruction resetC = new SetCarry(pc, ccr, false);
        Assert.assertEquals(2, resetC.getCycles());
        Assert.assertEquals(1, resetC.getSize());
        resetC.execute();

        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());
    }

    @Test
    public void testSetInterrupt() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        ccr.setC(false);
        ccr.setN(true);
        ccr.setV(true);
        ccr.setZ(false);

        Instruction setI = new SetInterrupt(pc, ccr, true);
        Assert.assertEquals(2, setI.getCycles());
        Assert.assertEquals(1, setI.getSize());
        setI.execute();

        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(true, ccr.getI());

        Instruction resetI = new SetInterrupt(pc, ccr, false);
        Assert.assertEquals(2, resetI.getCycles());
        Assert.assertEquals(1, resetI.getSize());
        resetI.execute();

        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getI());
    }

    @Test
    public void testSetOverflow() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        ccr.setC(false);
        ccr.setN(true);
        ccr.setV(false);
        ccr.setZ(false);

        Instruction setV = new SetOverflow(pc, ccr, true);
        Assert.assertEquals(2, setV.getCycles());
        Assert.assertEquals(1, setV.getSize());
        setV.execute();

        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getI());

        Instruction resetV = new SetOverflow(pc, ccr, false);
        Assert.assertEquals(2, resetV.getCycles());
        Assert.assertEquals(1, resetV.getSize());
        resetV.execute();

        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getI());
    }
}
