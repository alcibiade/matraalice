package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Assert;
import org.junit.Test;

public class PshPulTest {

    @Test
    public void testPSHByte() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        Register sp = new StaticRegister("SP", 16);
        CCR ccr = new CCR();
        Register a = new StaticRegister("A", 8);
        RandomAccessible memory = new MemoryArea(1 << 16);
        MemoryManager memMgr = new BigEndianMemoryManager(memory);
        Instruction psh = new Psh(pc, a, sp, memMgr);
        Instruction pul = new Pul(pc, a, sp, memMgr);

        sp.setValue(0xFFE0);
        a.setValue(0xBC);
        psh.execute();
        Assert.assertEquals((byte) 0xBC, memMgr.readByte(0xFFE0));
        Assert.assertEquals(0xFFDF, sp.getUnsignedValue());

        a.setValue(0x00);
        pul.execute();
        Assert.assertEquals((byte) 0xBC, a.getSignedValue());
        Assert.assertEquals((byte) 0xBC, memMgr.readByte(0xFFE0));
        Assert.assertEquals(0xFFE0, sp.getUnsignedValue());
    }

    @Test
    public void testPSHWord() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        Register sp = new StaticRegister("SP", 16);
        CCR ccr = new CCR();
        Register x = new StaticRegister("X", 16);
        RandomAccessible memory = new MemoryArea(1 << 16);
        MemoryManager memMgr = new BigEndianMemoryManager(memory);
        Instruction psh = new Psh(pc, x, sp, memMgr);
        Instruction pul = new Pul(pc, x, sp, memMgr);

        sp.setValue(0xFFE0);
        x.setValue(0xBCDE);
        psh.execute();
        Assert.assertEquals((byte) 0xDE, memMgr.readByte(0xFFE0));
        Assert.assertEquals((byte) 0xBC, memMgr.readByte(0xFFDF));
        Assert.assertEquals(0xFFDE, sp.getUnsignedValue());

        x.setValue(0x00);
        pul.execute();
        Assert.assertEquals((short) 0xBCDE, x.getSignedValue());
        Assert.assertEquals((short) 0xBCDE, memMgr.readWord(0xFFDF));
        Assert.assertEquals(0xFFE0, sp.getUnsignedValue());
    }
}
