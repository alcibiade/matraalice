package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Assert;
import org.junit.Test;

public class AddTest {

    @Test
    public void testAddImmediate() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register a = new StaticRegister("A", 8);

        RandomAccessible mem = new MemoryArea(12);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        MemoryAccessor memoryAccessor = new MemoryAccessor(memMgr, MemoryAccessMode.IMMEDIATE, pc,
                pc, 1);

        Instruction inst = new Add(pc, a, memoryAccessor, ccr, false);
        Assert.assertEquals(2, inst.getCycles());
        Assert.assertEquals(2, inst.getSize());

        ccr.setC(true);
        ccr.setV(true);
        mem.setByte(1, (byte) 0x3D);
        a.setValue(0x21);
        inst.execute();
        Assert.assertEquals(0x5E, a.getSignedValue());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getC());

        mem.setByte(3, (byte) 0x02);
        a.setValue(-5);
        inst.execute();
        Assert.assertEquals(-3, a.getSignedValue());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getC());

        mem.setByte(5, (byte) 0x05);
        a.setValue(-5);
        inst.execute();
        Assert.assertEquals(0, a.getSignedValue());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getC());

        mem.setByte(7, (byte) 100);
        a.setValue(100);
        inst.execute();
        Assert.assertEquals(-56, a.getSignedValue());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getC());

        mem.setByte(9, (byte) 130);
        a.setValue(130);
        inst.execute();
        Assert.assertEquals(4, a.getSignedValue());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(true, ccr.getC());
    }

    @Test
    public void testAddReg() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register a = new StaticRegister("A", 8);
        Register b = new StaticRegister("B", 8);

        Instruction inst = new Add(pc, a, b, ccr);
        Assert.assertEquals(2, inst.getCycles());
        Assert.assertEquals(1, inst.getSize());

        ccr.setC(true);
        ccr.setV(true);
        a.setValue((byte) 0x21);
        b.setValue((byte) 0x3D);
        inst.execute();
        Assert.assertEquals(0x5E, a.getSignedValue());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getC());
    }

    @Test
    public void testAddWithCarry() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register a = new StaticRegister("A", 8);

        RandomAccessible mem = new MemoryArea(12);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        MemoryAccessor memoryAccessor = new MemoryAccessor(memMgr, MemoryAccessMode.IMMEDIATE, pc,
                pc, 1);

        Instruction inst = new Add(pc, a, memoryAccessor, ccr, true);
        Assert.assertEquals(2, inst.getCycles());
        Assert.assertEquals(2, inst.getSize());

        ccr.setC(true);
        ccr.setV(true);
        mem.setByte(1, (byte) 0x3D);
        a.setValue(0x21);
        inst.execute();
        Assert.assertEquals(0x5F, a.getSignedValue());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getC());

        mem.setByte(3, (byte) 0x02);
        a.setValue(-5);
        inst.execute();
        Assert.assertEquals(-3, a.getSignedValue());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getC());

        mem.setByte(5, (byte) 0x05);
        a.setValue(-5);
        inst.execute();
        Assert.assertEquals(0, a.getSignedValue());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getC());

        mem.setByte(7, (byte) 100);
        a.setValue(100);
        inst.execute();
        Assert.assertEquals(-55, a.getSignedValue());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getC());

        mem.setByte(9, (byte) 130);
        a.setValue(130);
        inst.execute();
        Assert.assertEquals(4, a.getSignedValue());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(true, ccr.getC());
    }
}
