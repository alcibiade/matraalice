package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Assert;
import org.junit.Test;

public class EorTest {

    @Test
    public void testEorImmediate() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register a = new StaticRegister("A", 8);

        RandomAccessible mem = new MemoryArea(12);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        MemoryAccessor memoryAccessor = new MemoryAccessor(memMgr, MemoryAccessMode.IMMEDIATE, pc,
                pc, 1);

        Instruction inst = new Eor(pc, a, memoryAccessor, ccr);
        Assert.assertEquals(2, inst.getCycles());
        Assert.assertEquals(2, inst.getSize());

        // 0xFF ^ -4 = 

        ccr.setC(true);
        ccr.setV(true);
        mem.setByte(1, (byte) 0xFF);
        a.setValue(4);
        inst.execute();
        Assert.assertEquals(-5, a.getSignedValue());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getC());

        // 0x73 & 0x73 = 0x00

        mem.setByte(3, (byte) 0x73);
        a.setValue(0x73);
        inst.execute();
        Assert.assertEquals(0x00, a.getSignedValue());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getC());
    }

    @Test
    public void testEorExt() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        Register a = new StaticRegister("A", 8);
        a.setValue(0xF8);

        CCR ccr = new CCR();
        ccr.setC(true);
        ccr.setN(false);
        ccr.setV(true);
        ccr.setZ(false);

        RandomAccessible mem = new MemoryArea(1024);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        MemoryAccessor memoryAccessor = new MemoryAccessor(memMgr, MemoryAccessMode.EXTENDED, pc, pc,
                1);

        // Extended operand offset
        mem.setByte(1, (byte) 0x00);
        mem.setByte(2, (byte) 0x22);

        // Operand value
        mem.setByte(0x22, (byte) 0x8F);


        Instruction eor = new Eor(pc, a, memoryAccessor, ccr);
        Assert.assertEquals(4, eor.getCycles());
        Assert.assertEquals(3, eor.getSize());
        eor.execute();

        Assert.assertEquals(119, a.getUnsignedValue());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getV());
        // Carry is not affected by EOR operation
        Assert.assertEquals(true, ccr.getC());
    }
}
