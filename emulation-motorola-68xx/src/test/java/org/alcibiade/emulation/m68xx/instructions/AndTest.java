package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Assert;
import org.junit.Test;

public class AndTest {

    @Test
    public void testAndImmediate() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register a = new StaticRegister("A", 8);

        RandomAccessible mem = new MemoryArea(12);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        MemoryAccessor memoryAccessor = new MemoryAccessor(memMgr, MemoryAccessMode.IMMEDIATE, pc,
                pc, 1);

        Instruction inst = new And(pc, a, memoryAccessor, ccr);
        Assert.assertEquals(2, inst.getCycles());
        Assert.assertEquals(2, inst.getSize());

        // 0xFF & -4 = -4

        ccr.setC(true);
        ccr.setV(true);
        mem.setByte(1, (byte) 0xFF);
        a.setValue(-4);
        inst.execute();
        Assert.assertEquals(-4, a.getSignedValue());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getC());

        // 0x70 & 0x03 = 0x00

        mem.setByte(3, (byte) 0x70);
        a.setValue(0x03);
        inst.execute();
        Assert.assertEquals(0x00, a.getSignedValue());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getC());
    }

    @Test
    public void testANDExt() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        Register a = new StaticRegister("A", 8);
        a.setValue(0xF8);

        CCR ccr = new CCR();
        ccr.setC(true);
        ccr.setN(false);
        ccr.setV(true);
        ccr.setZ(false);

        RandomAccessible mem = new MemoryArea(1024);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        MemoryAccessor memoryAccessor = new MemoryAccessor(memMgr, MemoryAccessMode.EXTENDED, pc, pc,
                1);

        // Extended operand offset
        mem.setByte(1, (byte) 0x00);
        mem.setByte(2, (byte) 0x22);

        // Operand value
        mem.setByte(0x22, (byte) 0x8F);


        Instruction and = new And(pc, a, memoryAccessor, ccr);
        Assert.assertEquals(4, and.getCycles());
        Assert.assertEquals(3, and.getSize());
        and.execute();

        Assert.assertEquals(0x88, a.getUnsignedValue());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(false, ccr.getV());
        // Carry is not affected by AND operation
        Assert.assertEquals(true, ccr.getC());
    }
}
