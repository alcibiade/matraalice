package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.junit.Assert;
import org.junit.Test;

public class TabTest {

    @Test
    public void testCopy() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register a = new StaticRegister("A", 8);
        Register b = new StaticRegister("B", 8);

        Instruction inst = new Tab(pc, a, b, ccr);
        Assert.assertEquals(2, inst.getCycles());
        Assert.assertEquals(1, inst.getSize());

        ccr.setC(true);
        ccr.setV(true);
        a.setValue(-4);
        b.setValue(12);
        inst.execute();
        Assert.assertEquals(-4, a.getSignedValue());
        Assert.assertEquals(-4, b.getSignedValue());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getC());

    }
}
