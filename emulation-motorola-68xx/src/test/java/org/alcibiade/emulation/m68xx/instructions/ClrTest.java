package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Assert;
import org.junit.Test;

public class ClrTest {

    @Test
    public void testClrRegister() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        Register a = new StaticRegister("A", 8);
        a.setValue(0xD0);
        CCR ccr = new CCR();
        ccr.setC(true);
        ccr.setN(true);
        ccr.setV(true);
        ccr.setZ(false);

        Instruction clr = new Clr(pc, a, ccr);
        Assert.assertEquals(2, clr.getCycles());
        Assert.assertEquals(1, clr.getSize());
        clr.execute();

        Assert.assertEquals(0x00, a.getSignedValue());
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(false, ccr.getV());
    }

    @Test
    public void testClrMemory() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);

        CCR ccr = new CCR();
        ccr.setC(true);
        ccr.setN(true);
        ccr.setV(true);
        ccr.setZ(false);

        RandomAccessible mem = new MemoryArea(1024);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        MemoryAccessor memoryAccessor = new MemoryAccessor(memMgr, MemoryAccessMode.EXTENDED, pc, pc,
                1);

        // Extended offset
        mem.setByte(1, (byte) 0x00);
        mem.setByte(2, (byte) 0x22);
        // Initial value
        mem.setByte(0x22, (byte) 0xD0);

        Instruction clr = new Clr(pc, memoryAccessor, ccr);
        Assert.assertEquals(6, clr.getCycles());
        Assert.assertEquals(3, clr.getSize());
        clr.execute();

        Assert.assertEquals(0x00, mem.getByte(0x22));
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(false, ccr.getV());
    }
}
