package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Assert;
import org.junit.Test;

public class BitTest {

    @Test
    public void testBit() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register a = new StaticRegister("A", 8);

        RandomAccessible mem = new MemoryArea(32);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        MemoryAccessor memoryAccessor = new MemoryAccessor(memMgr, MemoryAccessMode.DIRECT, pc,
                pc, 1);

        // Set offset of the memory parameter to 1
        pc.setValue(16);
        memMgr.writeByte(17, (byte) 1);

        // Test the memory accessor configuration
        mem.setByte(1, (byte) 0xF0);
        Assert.assertEquals((byte) 0xF0, memoryAccessor.getSignedValue());

        Instruction inst = new Bit(pc, a, memoryAccessor, ccr);
        Assert.assertEquals(3, inst.getCycles());
        Assert.assertEquals(2, inst.getSize());

        // 0x08 & 0xF0 = 0x00 

        pc.setValue(16);
        ccr.setC(true);
        ccr.setV(true);
        ccr.setN(true);
        ccr.setZ(true);
        a.setValue(8);
        mem.setByte(1, (byte) 0xF0);
        inst.execute();
        Assert.assertEquals(8, a.getSignedValue());
        Assert.assertEquals((byte) 0xF0, mem.getByte(1));
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getC());

        // 0xF0 & 0xA2 = 0xA0 

        pc.setValue(16);
        ccr.setC(true);
        ccr.setV(true);
        ccr.setN(true);
        ccr.setZ(true);
        a.setValue(0xA2);
        mem.setByte(1, (byte) 0xF0);
        inst.execute();
        Assert.assertEquals((byte) 0xA2, a.getSignedValue());
        Assert.assertEquals((byte) 0xF0, mem.getByte(1));
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getC());
    }
}
