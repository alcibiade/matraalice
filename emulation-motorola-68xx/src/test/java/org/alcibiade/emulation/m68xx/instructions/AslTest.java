package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.CompositeRegister;
import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Assert;
import org.junit.Test;

public class AslTest {

    @Test
    public void testAslRegister() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        Register a = new StaticRegister("A", 8);

        CCR ccr = new CCR();
        ccr.setC(false);
        ccr.setN(false);
        ccr.setV(true);
        ccr.setZ(false);

        a.setValue(0xF8);

        Instruction asl = new Asl(pc, a, ccr);
        Assert.assertEquals(2, asl.getCycles());
        Assert.assertEquals(1, asl.getSize());
        asl.execute();

        Assert.assertEquals(0xF0, a.getUnsignedValue());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(true, ccr.getN());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getC());

        a.setValue(0x80);
        asl.execute();

        Assert.assertEquals(0x00, a.getUnsignedValue());
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(true, ccr.getC());

        a.setValue(0x00);
        asl.execute();

        Assert.assertEquals(0x00, a.getUnsignedValue());
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getC());
    }

    @Test
    public void testAslD() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        Register ra = new StaticRegister("A", 8);
        Register rb = new StaticRegister("A", 8);
        Register d = new CompositeRegister("D", ra, rb);

        CCR ccr = new CCR();
        ccr.setC(false);
        ccr.setN(false);
        ccr.setV(true);
        ccr.setZ(false);

        d.setValue(0xF8);

        Instruction asl = new Asl(pc, d, ccr);
        Assert.assertEquals(4, asl.getCycles());
        Assert.assertEquals(1, asl.getSize());
        asl.execute();

        Assert.assertEquals(0x01F0, d.getUnsignedValue());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getC());

        d.setValue(0x8000);
        asl.execute();

        Assert.assertEquals(0x00, d.getUnsignedValue());
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(true, ccr.getC());

        d.setValue(0x00);
        asl.execute();

        Assert.assertEquals(0x00, d.getUnsignedValue());
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getC());
    }

    @Test
    public void testAslMemory() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);

        CCR ccr = new CCR();
        ccr.setC(true);
        ccr.setN(false);
        ccr.setV(true);
        ccr.setZ(false);

        RandomAccessible mem = new MemoryArea(1024);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        MemoryAccessor memoryAccessor = new MemoryAccessor(memMgr, MemoryAccessMode.EXTENDED, pc, pc,
                1);

        // Extended operand offset
        mem.setByte(1, (byte) 0x00);
        mem.setByte(2, (byte) 0x22);

        // Operand value
        mem.setByte(0x22, (byte) 0x8F);


        Instruction asl = new Asl(pc, memoryAccessor, ccr);
        Assert.assertEquals(6, asl.getCycles());
        Assert.assertEquals(3, asl.getSize());
        asl.execute();

        Assert.assertEquals(0x1E, mem.getByte(0x22));
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(true, ccr.getC());
    }
}
