package org.alcibiade.emulation.m68xx;

import junit.framework.Assert;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.IllegalInstructionCodeException;
import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.alcibiade.emulation.m68xx.instructions.MemoryAccessMode;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class M6803Test {

    @Test
    public void testAccumulatorMapping() {
        M6803 cpu = new M6803();
        assertEquals("A", cpu.computeAccumulator(0x4F).getName());
        assertEquals("B", cpu.computeAccumulator(0x5F).getName());

        byte[] operationGroups = {0x00, 0x01, 0x02, 0x04, 0x05, 0x06, 0x08, 0x09, 0x0A, 0x0B};

        for (byte group : operationGroups) {
            assertEquals("A", cpu.computeAccumulator(0x80 | group).getName());
            assertEquals("A", cpu.computeAccumulator(0x90 | group).getName());
            assertEquals("A", cpu.computeAccumulator(0xA0 | group).getName());
            assertEquals("A", cpu.computeAccumulator(0xB0 | group).getName());
            assertEquals("B", cpu.computeAccumulator(0xC0 | group).getName());
            assertEquals("B", cpu.computeAccumulator(0xD0 | group).getName());
            assertEquals("B", cpu.computeAccumulator(0xE0 | group).getName());
            assertEquals("B", cpu.computeAccumulator(0xF0 | group).getName());
        }

        assertEquals("A", cpu.computeAccumulator(0x4D).getName());
        assertEquals("B", cpu.computeAccumulator(0x5D).getName());

        assertEquals("A", cpu.computeAccumulator(0x44).getName());
        assertEquals("B", cpu.computeAccumulator(0x54).getName());

        assertEquals("A", cpu.computeAccumulator(0x32).getName());
        assertEquals("B", cpu.computeAccumulator(0x33).getName());
        assertEquals("A", cpu.computeAccumulator(0x36).getName());
        assertEquals("B", cpu.computeAccumulator(0x37).getName());
    }

    @Test
    public void testAccessModeMapping() {
        for (int op = 0; op < 256; op++) {
            int ophigh = 0xF0 & op;
            MemoryAccessMode accessMode = M6803.computeAccessMode(op);

            if (ophigh == 0x20) {
                assertEquals(MemoryAccessMode.RELATIVE, accessMode);
            } else if (ophigh == 0x60) {
                assertEquals(MemoryAccessMode.INDEXED, accessMode);
            } else if (ophigh == 0x70) {
                assertEquals(MemoryAccessMode.EXTENDED, accessMode);
            } else if (ophigh == 0x80) {
                if (op == 0x8D) {
                    assertEquals(MemoryAccessMode.RELATIVE, accessMode);
                } else {
                    assertEquals(MemoryAccessMode.IMMEDIATE, accessMode);
                }
            } else if (ophigh == 0x90) {
                assertEquals(MemoryAccessMode.DIRECT, accessMode);
            } else if (ophigh == 0xA0) {
                assertEquals(MemoryAccessMode.INDEXED, accessMode);
            } else if (ophigh == 0xB0) {
                assertEquals(MemoryAccessMode.EXTENDED, accessMode);
            } else if (ophigh == 0xC0) {
                assertEquals(MemoryAccessMode.IMMEDIATE, accessMode);
            } else if (ophigh == 0xD0) {
                assertEquals(MemoryAccessMode.DIRECT, accessMode);
            } else if (ophigh == 0xE0) {
                assertEquals(MemoryAccessMode.INDEXED, accessMode);
            } else if (ophigh == 0xF0) {
                assertEquals(MemoryAccessMode.EXTENDED, accessMode);
            }
        }
    }

    @Test
    public void testCPX() throws IllegalMemoryAccessException, IllegalInstructionCodeException {
        RandomAccessible memory = new MemoryArea(64 * 1024);
        M6803 cpu = new M6803();
        cpu.setMemory(memory);
        cpu.init();

        memory.setByte(0, (byte) 0x8C);
        Instruction instruction = cpu.parseInstructionAt(0);

        assertEquals(3, instruction.getCycles());
        assertEquals(3, instruction.getSize());
    }

    @Test
    public void testROL() throws IllegalMemoryAccessException, IllegalInstructionCodeException {
        RandomAccessible memory = new MemoryArea(64 * 1024);
        M6803 cpu = new M6803();
        cpu.setMemory(memory);
        cpu.init();

        memory.setByte(0, (byte) 0x79);
        Instruction instruction = cpu.parseInstructionAt(0);

        assertEquals(6, instruction.getCycles());
        assertEquals(3, instruction.getSize());
    }

    @Test
    public void testJSR_Extended() throws IllegalMemoryAccessException,
            IllegalInstructionCodeException {
        RandomAccessible memory = new MemoryArea(64 * 1024);
        M6803 cpu = new M6803();
        cpu.setMemory(memory);
        cpu.init();

        memory.setByte(0, (byte) 0xBD);
        memory.setByte(1, (byte) 0xE3);
        memory.setByte(2, (byte) 0xDE);

        Instruction instruction = cpu.parseInstructionAt(0);

        assertEquals(9, instruction.getCycles());
        assertEquals(3, instruction.getSize());
    }

    @Test
    public void testJSR_Indexed() throws IllegalMemoryAccessException,
            IllegalInstructionCodeException {
        RandomAccessible memory = new MemoryArea(64 * 1024);
        M6803 cpu = new M6803();
        cpu.setMemory(memory);
        cpu.init();

        memory.setByte(0, (byte) 0xAD);
        memory.setByte(1, (byte) 0xE3);

        Instruction instruction = cpu.parseInstructionAt(0);

        assertEquals(8, instruction.getCycles());
        assertEquals(2, instruction.getSize());
    }

    @Test
    public void testJMP_Indexed() throws IllegalMemoryAccessException,
            IllegalInstructionCodeException {
        RandomAccessible memory = new MemoryArea(64 * 1024);
        M6803 cpu = new M6803();
        cpu.setMemory(memory);
        cpu.init();

        // LDX 0x0030
        memory.setByte(0x00, (byte) 0xCE);
        memory.setByte(0x01, (byte) 0x00);
        memory.setByte(0x02, (byte) 0x30);

        // JMP X+1
        memory.setByte(0x03, (byte) 0x6E);
        memory.setByte(0x04, (byte) 0x01);


        int durationCycles1 = cpu.runCycle();
        assertEquals(3, durationCycles1);
        assertEquals(3, cpu.getProgramAddress());

        int durationCycles2 = cpu.runCycle();
        assertEquals(4, durationCycles2);
        assertEquals(0x31, cpu.getProgramAddress());
    }

    @Test
    public void testJMP_Extended() throws IllegalMemoryAccessException,
            IllegalInstructionCodeException {
        RandomAccessible memory = new MemoryArea(64 * 1024);
        M6803 cpu = new M6803();
        cpu.setMemory(memory);
        cpu.init();

        // JMP 30
        memory.setByte(0x00, (byte) 0x7E);
        memory.setByte(0x01, (byte) 0x00);
        memory.setByte(0x02, (byte) 0x30);

        int durationCycles1 = cpu.runCycle();
        assertEquals(3, durationCycles1);
        assertEquals(0x30, cpu.getProgramAddress());
    }

    @Test
    public void testABX() throws IllegalMemoryAccessException,
            IllegalInstructionCodeException {
        RandomAccessible memory = new MemoryArea(64 * 1024);
        M6803 cpu = new M6803();
        cpu.setMemory(memory);
        cpu.init();

        // LDX 0x0040
        memory.setByte(0x00, (byte) 0xCE);
        memory.setByte(0x01, (byte) 0x00);
        memory.setByte(0x02, (byte) 0x40);
        // LDAB 0x0D
        memory.setByte(0x03, (byte) 0xC6);
        memory.setByte(0x04, (byte) 0x0D);
        // ABX
        memory.setByte(0x05, (byte) 0x3A);
        // STX [20h]
        memory.setByte(0x06, (byte) 0xFF);
        memory.setByte(0x07, (byte) 0x00);
        memory.setByte(0x08, (byte) 0x20);

        assertEquals(3, cpu.runCycle());
        assertEquals(0x03, cpu.getProgramAddress());

        assertEquals(2, cpu.runCycle());
        assertEquals(0x05, cpu.getProgramAddress());

        assertEquals(3, cpu.runCycle());
        assertEquals(0x06, cpu.getProgramAddress());

        assertEquals(6, cpu.runCycle());
        assertEquals(0x09, cpu.getProgramAddress());

        assertEquals(0x4D, memory.getByte(0x21));
    }

    @Test
    public void testTST_Register() throws IllegalMemoryAccessException,
            IllegalInstructionCodeException {
        RandomAccessible memory = new MemoryArea(64 * 1024);
        M6803 cpu = new M6803();
        cpu.setMemory(memory);
        cpu.init();

        // LDAA 0x0D
        memory.setByte(0x00, (byte) 0x86);
        memory.setByte(0x01, (byte) 0x0D);
        // TSTA
        memory.setByte(0x02, (byte) 0x4D);
        // BNE 0x10
        memory.setByte(0x03, (byte) 0x26);
        memory.setByte(0x04, (byte) 0x10);
        // TSTB
        memory.setByte(0x15, (byte) 0x5D);
        // BNE 0x10
        memory.setByte(0x16, (byte) 0x26);
        memory.setByte(0x17, (byte) 0x10);

        assertEquals(2, cpu.runCycle());
        assertEquals(0x02, cpu.getProgramAddress());

        assertEquals(2, cpu.runCycle());
        assertEquals(0x03, cpu.getProgramAddress());

        assertEquals(4, cpu.runCycle());
        assertEquals(0x15, cpu.getProgramAddress());

        assertEquals(2, cpu.runCycle());
        assertEquals(0x16, cpu.getProgramAddress());

        assertEquals(4, cpu.runCycle());
        assertEquals(0x18, cpu.getProgramAddress());
    }

    @Test
    public void testTST_Memory() throws IllegalMemoryAccessException,
            IllegalInstructionCodeException {
        RandomAccessible memory = new MemoryArea(64 * 1024);
        M6803 cpu = new M6803();
        cpu.setMemory(memory);
        cpu.init();

        memory.setByte(0x34, (byte) 0x4F);


        // LDX 0x0030
        memory.setByte(0x00, (byte) 0xCE);
        memory.setByte(0x01, (byte) 0x00);
        memory.setByte(0x02, (byte) 0x30);

        // TST [x+4]
        memory.setByte(0x03, (byte) 0x6D);
        memory.setByte(0x04, (byte) 0x04);

        // BNE 0x10
        memory.setByte(0x05, (byte) 0x26);
        memory.setByte(0x06, (byte) 0x10);

        // TST [34h]
        memory.setByte(0x17, (byte) 0x7D);
        memory.setByte(0x18, (byte) 0x00);
        memory.setByte(0x19, (byte) 0x34);

        // BNE 0x10
        memory.setByte(0x1A, (byte) 0x26);
        memory.setByte(0x1B, (byte) 0x10);

        assertEquals(3, cpu.runCycle());
        assertEquals(0x03, cpu.getProgramAddress());

        assertEquals(7, cpu.runCycle());
        assertEquals(0x05, cpu.getProgramAddress());

        assertEquals(4, cpu.runCycle());
        assertEquals(0x17, cpu.getProgramAddress());

        assertEquals(6, cpu.runCycle());
        assertEquals(0x1A, cpu.getProgramAddress());

        assertEquals(4, cpu.runCycle());
        assertEquals(0x2C, cpu.getProgramAddress());
    }

    @Test
    public void testEOR() throws IllegalMemoryAccessException,
            IllegalInstructionCodeException {
        RandomAccessible memory = new MemoryArea(64 * 1024);
        M6803 cpu = new M6803();
        cpu.setMemory(memory);
        cpu.init();

        // EOR A, $6Bh
        memory.setByte(0x00, (byte) 0x88);
        memory.setByte(0x01, (byte) 0x6B);
        // STA A, 0x50
        memory.setByte(0x02, (byte) 0xB7);
        memory.setByte(0x03, (byte) 0x00);
        memory.setByte(0x04, (byte) 0x50);
        // EOR A, $01h
        memory.setByte(0x05, (byte) 0x88);
        memory.setByte(0x06, (byte) 0x01);
        // STA A, 0x51
        memory.setByte(0x07, (byte) 0xB7);
        memory.setByte(0x08, (byte) 0x00);
        memory.setByte(0x09, (byte) 0x51);
        // EOR B, [0051h]
        memory.setByte(0x0A, (byte) 0xF8);
        memory.setByte(0x0B, (byte) 0x00);
        memory.setByte(0x0C, (byte) 0x51);
        // STA B, 0x52
        memory.setByte(0x0D, (byte) 0xF7);
        memory.setByte(0x0E, (byte) 0x00);
        memory.setByte(0x0F, (byte) 0x52);

        assertEquals(2, cpu.runCycle());
        assertEquals(0x02, cpu.getProgramAddress());
        assertEquals(5, cpu.runCycle());
        assertEquals(0x05, cpu.getProgramAddress());
        assertEquals(2, cpu.runCycle());
        assertEquals(0x07, cpu.getProgramAddress());
        assertEquals(5, cpu.runCycle());
        assertEquals(0x0A, cpu.getProgramAddress());
        assertEquals(4, cpu.runCycle());
        assertEquals(0x0D, cpu.getProgramAddress());
        assertEquals(5, cpu.runCycle());
        assertEquals(0x10, cpu.getProgramAddress());

        assertEquals(0x6B, memory.getByte(0x50));
        assertEquals(0x6A, memory.getByte(0x51));
        assertEquals(0x6A, memory.getByte(0x52));
    }

    @Test
    public void testPush() throws IllegalMemoryAccessException,
            IllegalInstructionCodeException {
        RandomAccessible memory = new MemoryArea(64 * 1024);
        M6803 cpu = new M6803();
        cpu.setMemory(memory);
        cpu.init();

        // LDS $0100
        memory.setByte(0x00, (byte) 0x8E);
        memory.setByte(0x01, (byte) 0x01);
        memory.setByte(0x02, (byte) 0x00);
        // ADD A, $4Dh
        memory.setByte(0x03, (byte) 0x8B);
        memory.setByte(0x04, (byte) 0x4D);
        // PSH A
        memory.setByte(0x05, (byte) 0x36);
        // PUL B
        memory.setByte(0x06, (byte) 0x33);
        // STA A, 0x50
        memory.setByte(0x07, (byte) 0xB7);
        memory.setByte(0x08, (byte) 0x00);
        memory.setByte(0x09, (byte) 0x50);
        // STA A, 0x50
        memory.setByte(0x0A, (byte) 0xF7);
        memory.setByte(0x0B, (byte) 0x00);
        memory.setByte(0x0C, (byte) 0x51);


        assertEquals(3, cpu.runCycle());
        assertEquals(0x03, cpu.getProgramAddress());
        assertEquals(2, cpu.runCycle());
        assertEquals(0x05, cpu.getProgramAddress());
        assertEquals(4, cpu.runCycle());
        assertEquals(0x06, cpu.getProgramAddress());
        assertEquals(4, cpu.runCycle());
        assertEquals(0x07, cpu.getProgramAddress());
        assertEquals(5, cpu.runCycle());
        assertEquals(0x0A, cpu.getProgramAddress());
        assertEquals(5, cpu.runCycle());
        assertEquals(0x0D, cpu.getProgramAddress());

        assertEquals(0x4D, memory.getByte(0x50));
        assertEquals(0x4D, memory.getByte(0x51));
    }

    @Test
    public void testBSR() throws IllegalMemoryAccessException,
            IllegalInstructionCodeException {
        RandomAccessible memory = new MemoryArea(64 * 1024);
        M6803 cpu = new M6803();
        cpu.setMemory(memory);
        cpu.init();

        Register sp = cpu.getRegisterSP();
        sp.setValue(0xEFFF);

        // NOP
        memory.setByte(0x00, (byte) 0x01);

        // BSR +10h
        memory.setByte(0x01, (byte) 0x8D);
        memory.setByte(0x02, (byte) 0x10);

        // INC A
        memory.setByte(0x13, (byte) 0x4C);
        // RTS 
        memory.setByte(0x14, (byte) 0x39);

        cpu.runCycle();
        assertEquals(0x01, cpu.getProgramAddress());
        assertEquals(0xEFFF, sp.getUnsignedValue());

        cpu.runCycle();
        assertEquals(0x13, cpu.getProgramAddress());
        assertEquals(0xEFFD, sp.getUnsignedValue());
        assertEquals(0x00, memory.getByte(0xEFFE));
        assertEquals(0x03, memory.getByte(0xEFFF));

        cpu.runCycle();
        assertEquals(0x14, cpu.getProgramAddress());
        assertEquals(0xEFFD, sp.getUnsignedValue());

        cpu.runCycle();
        assertEquals(0x03, cpu.getProgramAddress());
        assertEquals(0xEFFF, sp.getUnsignedValue());
    }

    @Test
    public void testInxAndDex() throws IllegalMemoryAccessException,
            IllegalInstructionCodeException {
        RandomAccessible memory = new MemoryArea(64 * 1024);
        M6803 cpu = new M6803();
        cpu.setMemory(memory);
        cpu.init();

        // INX
        memory.setByte(0x00, (byte) 0x08);
        memory.setByte(0x01, (byte) 0x08);
        memory.setByte(0x02, (byte) 0x08);
        memory.setByte(0x03, (byte) 0x08);

        cpu.runCycle();
        cpu.runCycle();
        cpu.runCycle();
        cpu.runCycle();

        Assert.assertEquals(false, cpu.getRegisterCCR().getZ());
        Assert.assertEquals(4, cpu.getRegisterX().getSignedValue());

        // DEX
        memory.setByte(0x04, (byte) 0x09);
        memory.setByte(0x05, (byte) 0x09);
        memory.setByte(0x06, (byte) 0x09);
        memory.setByte(0x07, (byte) 0x09);

        cpu.runCycle();
        cpu.runCycle();
        cpu.runCycle();
        cpu.runCycle();

        Assert.assertEquals(true, cpu.getRegisterCCR().getZ());
        Assert.assertEquals(0, cpu.getRegisterX().getSignedValue());
    }

    @Test
    public void testInsAndDes() throws IllegalMemoryAccessException,
            IllegalInstructionCodeException {
        RandomAccessible memory = new MemoryArea(64 * 1024);
        M6803 cpu = new M6803();
        cpu.setMemory(memory);
        cpu.init();

        Register sp = cpu.getRegisterSP();
        sp.setValue(0x0000);

        // INS
        memory.setByte(0x00, (byte) 0x31);
        memory.setByte(0x01, (byte) 0x31);
        memory.setByte(0x02, (byte) 0x31);
        memory.setByte(0x03, (byte) 0x31);

        cpu.runCycle();
        cpu.runCycle();
        cpu.runCycle();
        cpu.runCycle();

        Assert.assertEquals(4, cpu.getRegisterSP().getSignedValue());
        Assert.assertEquals(false, cpu.getRegisterCCR().getZ());

        // DES
        memory.setByte(0x04, (byte) 0x34);
        memory.setByte(0x05, (byte) 0x34);
        memory.setByte(0x06, (byte) 0x34);
        memory.setByte(0x07, (byte) 0x34);

        cpu.runCycle();
        cpu.runCycle();
        cpu.runCycle();
        cpu.runCycle();

        Assert.assertEquals(0, cpu.getRegisterSP().getSignedValue());
        Assert.assertEquals(false, cpu.getRegisterCCR().getZ());
    }
}
