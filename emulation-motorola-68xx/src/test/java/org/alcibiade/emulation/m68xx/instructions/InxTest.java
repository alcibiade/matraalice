package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.junit.Assert;
import org.junit.Test;

public class InxTest {

    @Test
    public void testINX() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register x = new StaticRegister("X", 16);
        Instruction inc = new Inx(pc, x, ccr, true);
        Assert.assertEquals(4, inc.getCycles());
        Assert.assertEquals(1, inc.getSize());

        ccr.setZ(true);
        ccr.setC(true);
        ccr.setV(true);
        x.setValue(-2);
        inc.execute();
        Assert.assertEquals(-1, x.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());

        inc.execute();
        Assert.assertEquals(0, x.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(true, ccr.getZ());

        inc.execute();
        Assert.assertEquals(1, x.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());

        x.setValue(32767);
        inc.execute();
        Assert.assertEquals(-32768, x.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());
    }
    
    @Test
    public void testINS() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register s = new StaticRegister("SP", 16);
        Instruction inc = new Inx(pc, s, ccr, false);
        Assert.assertEquals(4, inc.getCycles());
        Assert.assertEquals(1, inc.getSize());

        ccr.setZ(true);
        ccr.setC(true);
        ccr.setV(true);
        s.setValue(-2);
        inc.execute();
        Assert.assertEquals(-1, s.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(true, ccr.getZ());

        inc.execute();
        Assert.assertEquals(0, s.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(true, ccr.getZ());

        inc.execute();
        Assert.assertEquals(1, s.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(true, ccr.getZ());

        s.setValue(32767);
        inc.execute();
        Assert.assertEquals(-32768, s.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(true, ccr.getZ());
    }
}
