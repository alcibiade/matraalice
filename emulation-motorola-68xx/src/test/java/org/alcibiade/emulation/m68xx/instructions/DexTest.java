package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.junit.Assert;
import org.junit.Test;

public class DexTest {

    @Test
    public void testDEX() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register x = new StaticRegister("X", 16);
        Instruction dec = new Dex(pc, x, ccr, true);
        Assert.assertEquals(4, dec.getCycles());
        Assert.assertEquals(1, dec.getSize());

        ccr.setZ(true);
        ccr.setC(true);
        ccr.setV(true);
        x.setValue(2);
        dec.execute();
        Assert.assertEquals(1, x.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());

        dec.execute();
        Assert.assertEquals(0, x.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(true, ccr.getZ());

        dec.execute();
        Assert.assertEquals(-1, x.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());

        x.setValue(-32768);
        dec.execute();
        Assert.assertEquals(32767, x.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());
    }
}
