package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Assert;
import org.junit.Test;

public class LdaTest {

    @Test
    public void testLDA() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register a = new StaticRegister("A", 8);
        RandomAccessible memory = new MemoryArea(1 << 16);
        MemoryManager memMgr = new BigEndianMemoryManager(memory);
        MemoryAccessor memAccessor = new MemoryAccessor(memMgr,
                MemoryAccessMode.EXTENDED, pc, null, 1);
        Instruction lda = new Lda(pc, a, memAccessor, ccr);

        // Load address set to 0xFFF0
        memMgr.writeWord(1, (short) 0xFFF0);
        // Write data 0xDA
        memMgr.writeByte(0xFFF0, (byte) 0xDA);

        lda.execute();
        Assert.assertEquals((byte) 0xDA, a.getSignedValue());
        Assert.assertEquals(false, ccr.getZ());

        // Write data 0x00
        memMgr.writeByte(0xFFF0, (byte) 0x00);
        lda.execute();
        Assert.assertEquals((byte) 0x00, a.getSignedValue());
        Assert.assertEquals(true, ccr.getZ());
    }
}
