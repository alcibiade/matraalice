package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Assert;
import org.junit.Test;

public class RtsTest {

    @Test
    public void testRtsOperation() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        Register sp = new StaticRegister("SP", 16);
        sp.setValue(0xEFFD);
        CCR ccr = new CCR();
        RandomAccessible mem = new MemoryArea(64 * 1024);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);

        mem.setByte(0x0EFFF, (byte) 0x12);

        Instruction bsr = new Rts(pc, sp, memMgr);
        bsr.execute();

        Assert.assertEquals(0xEFFF, sp.getUnsignedValue());
        Assert.assertEquals(0x0012, pc.getUnsignedValue());
    }
}
