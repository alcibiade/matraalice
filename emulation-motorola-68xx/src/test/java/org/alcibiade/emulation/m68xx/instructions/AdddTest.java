package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Assert;
import org.junit.Test;

public class AdddTest {

    @Test
    public void testAdddExtended() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register d = new StaticRegister("D", 16);

        RandomAccessible mem = new MemoryArea(1024);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        MemoryAccessor memoryAccessor = new MemoryAccessor(memMgr, MemoryAccessMode.EXTENDED, pc,
                pc, 2);

        Instruction inst = new Addd(pc, d, memoryAccessor, ccr);
        Assert.assertEquals(6, inst.getCycles());
        Assert.assertEquals(3, inst.getSize());

        ccr.setC(true);
        ccr.setV(true);

        // Operand 1

        d.setValue(0x20);

        // Operand 2

        mem.setByte(1, (byte) 0x00);
        mem.setByte(2, (byte) 0x1E);
        mem.setByte(0x1E, (byte) 0x10);
        mem.setByte(0x1F, (byte) 0xAA);

        inst.execute();

        Assert.assertEquals(0x10CA, d.getUnsignedValue());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getC());
    }

    @Test
    public void testAdddImmediate() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register d = new StaticRegister("D", 16);

        RandomAccessible mem = new MemoryArea(1024);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        MemoryAccessor memoryAccessor = new MemoryAccessor(memMgr, MemoryAccessMode.IMMEDIATE, pc,
                pc, 2);

        Instruction inst = new Addd(pc, d, memoryAccessor, ccr);
        Assert.assertEquals(4, inst.getCycles());
        Assert.assertEquals(3, inst.getSize());

        ccr.setC(true);
        ccr.setV(true);

        // Operand 1

        d.setValue(0x20);

        // Operand 2

        mem.setByte(1, (byte) 0x10);
        mem.setByte(2, (byte) 0xAA);

        inst.execute();

        Assert.assertEquals(0x10CA, d.getUnsignedValue());
        Assert.assertEquals(false, ccr.getN());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getC());
    }
}
