package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryArea;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.junit.Assert;
import org.junit.Test;

public class NegTest {

    @Test
    public void testNEGReg() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 16);
        CCR ccr = new CCR();
        Register a = new StaticRegister("A", 8);
        Instruction neg = new Neg(pc, a, ccr);
        Assert.assertEquals(2, neg.getCycles());
        Assert.assertEquals(1, neg.getSize());

        ccr.setC(true);
        a.setValue(4);
        neg.execute();
        Assert.assertEquals(-4, a.getSignedValue());
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(true, ccr.getN());

        ccr.setC(false);
        a.setValue(-4);
        neg.execute();
        Assert.assertEquals(4, a.getSignedValue());
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getN());

        a.setValue(0x00);
        neg.execute();
        Assert.assertEquals(0, a.getSignedValue());
        Assert.assertEquals(true, ccr.getC());
        Assert.assertEquals(true, ccr.getZ());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getN());

        a.setValue(0x80);
        neg.execute();
        Assert.assertEquals(0x80, a.getUnsignedValue());
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(false, ccr.getZ());
        Assert.assertEquals(true, ccr.getV());
        Assert.assertEquals(true, ccr.getN());
    }

    @Test
    public void testNEGMem() throws IllegalMemoryAccessException {
        Register pc = new StaticRegister("PC", 8);
        CCR ccr = new CCR();
        RandomAccessible mem = new MemoryArea(12);
        MemoryManager memMgr = new BigEndianMemoryManager(mem);
        Instruction neg = new Neg(pc,
                new MemoryAccessor(memMgr, MemoryAccessMode.EXTENDED, pc, pc, 1), ccr);
        Assert.assertEquals(6, neg.getCycles());
        Assert.assertEquals(3, neg.getSize());

        ccr.setC(false);
        mem.setByte(0, (byte) -4);
        neg.execute();
        Assert.assertEquals(4, mem.getByte(0));
        Assert.assertEquals(false, ccr.getC());
        Assert.assertEquals(false, ccr.getV());
        Assert.assertEquals(false, ccr.getZ());
    }
}
