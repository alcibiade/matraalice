package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryManager;

public class Rts extends AbstractInstruction {

    private Register regSP;
    private Register regPC;
    private MemoryManager mem;

    public Rts(Register regPC, Register regSP, MemoryManager mem) {
        super(regPC, "RTS", "");
        this.regPC = regPC;
        this.regSP = regSP;
        this.mem = mem;
    }

    @Override
    public int getCycles() {
        return 5;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int spOffset = regSP.getUnsignedValue();
        int returnOffset = mem.readWord(spOffset + 1);
        regSP.setValue(spOffset + 2);
        regPC.setValue(returnOffset);
    }
}
