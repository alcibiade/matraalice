package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class And extends AbstractInstruction {

    private Register reg;
    private MemoryAccessor mem;
    private CCR ccr;

    public And(Register pc, Register reg, MemoryAccessor mem, CCR regCCR) {
        super(pc, "AND " + reg.getName() + ", " + mem, reg.getName() + " . M -> " + reg.getName());
        this.reg = reg;
        this.mem = mem;
        this.ccr = regCCR;
    }

    @Override
    public int getCycles() {
        return getCycles(mem.getMode());
    }

    @Override
    public int getSize() {
        return getSize(mem.getMode());
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int registerValue = reg.getSignedValue();
        int memoryValue = mem.getSignedValue();
        int value = registerValue & memoryValue;
        reg.setValue(value);
        updateCCRN(ccr, value, 1);
        updateCCRZ(ccr, value, 1);
        ccr.setV(false);
        advancePC();
    }
}
