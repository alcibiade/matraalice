package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Cmp extends AbstractInstruction {

    private Register reg;
    private MemoryAccessor mem;
    private CCR ccr;

    public Cmp(Register pc, Register reg, MemoryAccessor mem, CCR regCCR) {
        super(pc, "CMP " + reg.getName() + ", " + mem, reg.getName() + " - M");
        this.reg = reg;
        this.mem = mem;
        this.ccr = regCCR;
    }

    @Override
    public int getCycles() {
        return getCycles(mem.getMode());
    }

    @Override
    public int getSize() {
        return getSize(mem.getMode());
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int firstOp = reg.getUnsignedValue();
        int secondOp = mem.getUnsignedValue();
        int value = firstOp - secondOp;

        updateCCRN(ccr, value, 1);
        updateCCRZ(ccr, value, 1);
        updateCCRV8(ccr, firstOp, secondOp, value);
        updateCCRC(ccr, value, 1);

        advancePC();
    }
}
