package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Bne extends BranchInstruction {

    public Bne(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "BNE", "Z = 0");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return !ccr.getZ();
    }
}
