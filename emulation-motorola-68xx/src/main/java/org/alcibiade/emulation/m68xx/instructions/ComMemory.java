package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class ComMemory extends AbstractInstruction {

    private MemoryAccessor mem;
    private CCR ccr;

    public ComMemory(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, "COM " + mem, "~M -> M");
        this.mem = mem;
        this.ccr = regCCR;
    }

    @Override
    public int getCycles() {
        return 2 + getCycles(mem.getMode());
    }

    @Override
    public int getSize() {
        return getSize(mem.getMode());
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int value = ~mem.getSignedValue();
        mem.setValue(value);
        updateCCRN(ccr, value, 1);
        updateCCRZ(ccr, value, 1);
        ccr.setV(false);
        ccr.setC(true);
        advancePC();
    }
}
