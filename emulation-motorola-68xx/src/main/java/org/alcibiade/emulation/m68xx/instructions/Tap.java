package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Tap extends AbstractInstruction {

    private Register reg;
    private CCR ccr;

    public Tap(Register pc, Register reg, CCR regCCR) {
        super(pc, "TAP", "A -> CCR");
        this.reg = reg;
        this.ccr = regCCR;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        ccr.setValue(reg.getSignedValue());
        advancePC();
    }
}
