package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public abstract class BranchInstruction extends AbstractInstruction {

    private CCR ccr;
    private MemoryAccessor mem;

    public BranchInstruction(Register pc, MemoryAccessor mem, CCR regCCR, String opCode,
            String notes) {
        super(pc, opCode + " " + mem, notes);
        this.ccr = regCCR;
        this.mem = mem;
    }

    @Override
    public int getCycles() {
        return mem.getMode() == MemoryAccessMode.EXTENDED ? 3 : 4;
    }

    @Override
    public int getSize() {
        return mem.getMode() == MemoryAccessMode.EXTENDED ? 3 : 2;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        if (decideBranch(ccr)) {
            advancePC(mem.getAddress());
        } else {
            advancePC();
        }
    }

    protected abstract boolean decideBranch(CCR ccr);
}
