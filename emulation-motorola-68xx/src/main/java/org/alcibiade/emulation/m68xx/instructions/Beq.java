package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Beq extends BranchInstruction {

    public Beq(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "BEQ", "Z = 1");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return ccr.getZ();
    }
}
