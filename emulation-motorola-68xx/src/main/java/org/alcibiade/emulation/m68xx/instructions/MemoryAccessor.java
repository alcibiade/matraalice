package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.BinUtils;
import org.alcibiade.emulation.core.cpu.DataAccessor;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryManager;

public class MemoryAccessor implements DataAccessor {

    private MemoryAccessMode mode;
    private MemoryManager memoryManager;
    private Register regPC;
    private Register regX;
    private int bytes;

    public MemoryAccessor(MemoryManager manager, MemoryAccessMode mode, Register regPC,
            Register regX, int bytes) {
        this.mode = mode;
        this.memoryManager = manager;
        this.regPC = regPC;
        this.regX = regX;
        this.bytes = bytes;
    }

    @Override
    public int getSignedValue() throws IllegalMemoryAccessException {
        int value = getRawValue();
        return BinUtils.convertToSigned(value, bytes * 8);
    }

    @Override
    public int getUnsignedValue() throws IllegalMemoryAccessException {
        int value = getRawValue();
        return BinUtils.convertToUnsigned(value, bytes * 8);
    }

    private int getRawValue() throws IllegalMemoryAccessException {
        int address = getAddress();
        int value = 0;

        switch (bytes) {
            case 1:
                value = memoryManager.readByte(address);
                break;
            default:
                value = memoryManager.readWord(address);
        }

        return value;
    }

    @Override
    public void setValue(int value) throws IllegalMemoryAccessException {
        int address = getAddress();

        switch (bytes) {
            case 1:
                memoryManager.writeByte(address, (byte) value);
                break;
            default:
                memoryManager.writeWord(address, (short) value);
        }
    }

    public MemoryAccessMode getMode() {
        return mode;
    }

    public int getAddress() throws IllegalMemoryAccessException {
        int address = 0;
        int parameter = getParameter();

        switch (mode) {
            case DIRECT:
            case EXTENDED:
                address = parameter;
                break;
            case IMMEDIATE:
                address = regPC.getUnsignedValue() + 1;
                break;
            case INDEXED:
                address = regX.getUnsignedValue() + (byte) parameter;
                break;
            case RELATIVE:
                address = regPC.getUnsignedValue() + 2 + (byte) parameter;
                break;
        }

        return address;
    }

    private int getParameter() throws IllegalMemoryAccessException {
        int parameter = 0;

        switch (mode) {
            case DIRECT:
            case INDEXED:
                parameter = BinUtils.convertToUnsigned(
                        memoryManager.readByte(regPC.getUnsignedValue() + 1), 8);
                break;
            case RELATIVE:
                parameter = memoryManager.readByte(regPC.getUnsignedValue() + 1);
                break;
            case EXTENDED:
                parameter = BinUtils.convertToUnsigned(
                        memoryManager.readWord(regPC.getUnsignedValue() + 1), 16);
                break;
            case IMMEDIATE:
                if (bytes == 1) {
                    parameter = memoryManager.readByte(regPC.getUnsignedValue() + 1);
                } else {
                    parameter = memoryManager.readWord(regPC.getUnsignedValue() + 1);
                }
                break;
        }

        return parameter;
    }

    @Override
    public String getName() {
        return toString();
    }

    @Override
    public String toString() {
        String text = super.toString();

        try {
            String format;
            int parameter = getParameter();

            switch (mode) {
                case DIRECT:
                    text = String.format("[%02Xh]", parameter);
                    break;
                case EXTENDED:
                    text = String.format("[%04Xh]", parameter);
                    break;
                case IMMEDIATE:
                    format = bytes == 1 ? "$%02Xh" : "$%04Xh";
                    text = String.format(format, parameter);
                    break;
                case INDEXED:
                    text = String.format("[X:%02Xh][%04Xh]", parameter, getAddress());
                    break;
                case RELATIVE:
                    format = "%04Xh";
                    parameter = getAddress();
                    text = String.format(format, parameter);
                    break;
            }
        } catch (IllegalMemoryAccessException e) {
            // Do nothing
        }

        return text;
    }
}
