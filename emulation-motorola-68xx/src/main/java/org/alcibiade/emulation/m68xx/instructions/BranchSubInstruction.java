package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryManager;

public abstract class BranchSubInstruction extends AbstractInstruction {

    private CCR ccr;
    private Register regSP;
    private Register regPC;
    private MemoryManager mem;
    private MemoryAccessor parameter;

    public BranchSubInstruction(Register regPC, MemoryAccessor parameter, CCR regCCR, String opCode,
            String notes, Register regSP, MemoryManager mem) {
        super(regPC, opCode, notes);
        this.regPC = regPC;
        this.ccr = regCCR;
        this.mem = mem;
        this.regSP = regSP;
        this.parameter = parameter;
    }

    @Override
    public int getCycles() {
        return parameter.getMode() == MemoryAccessMode.INDEXED ? 8 : 9;
    }

    @Override
    public int getSize() {
        return getSize(parameter.getMode());
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        if (decideBranch(ccr)) {
            int spOffset = regSP.getUnsignedValue();
            int nextIntOffset = regPC.getUnsignedValue() + getSize();

            mem.writeWord(spOffset - 1, (short) nextIntOffset);
            regSP.setValue(spOffset - 2);

            advancePC(parameter.getAddress());
        } else {
            advancePC();
        }
    }

    protected abstract boolean decideBranch(CCR ccr);
}
