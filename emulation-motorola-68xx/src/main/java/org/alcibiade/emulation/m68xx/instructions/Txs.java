package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Txs extends AbstractInstruction {

    private Register regX;
    private Register regSP;
    private CCR ccr;

    public Txs(Register pc, Register regX, Register regSP, CCR regCCR) {
        super(pc, "TXS", "X - 1 -> SP");
        this.regX = regX;
        this.regSP = regSP;
        this.ccr = regCCR;
    }

    @Override
    public int getCycles() {
        return 4;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int x = regX.getUnsignedValue();
        regSP.setValue(x - 1);
        advancePC();
    }
}
