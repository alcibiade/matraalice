package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Tsx extends AbstractInstruction {

    private Register regX;
    private Register regSP;
    private CCR ccr;

    public Tsx(Register pc, Register regX, Register regSP, CCR regCCR) {
        super(pc, "TSX", "SP + 1 -> X");
        this.regX = regX;
        this.regSP = regSP;
        this.ccr = regCCR;
    }

    @Override
    public int getCycles() {
        return 4;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int sp = regSP.getUnsignedValue();
        regX.setValue(sp + 1);
        advancePC();
    }
}
