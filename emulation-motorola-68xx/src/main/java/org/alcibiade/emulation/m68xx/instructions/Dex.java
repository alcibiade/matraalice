package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Dex extends AbstractInstruction {

    private Register reg;
    private MemoryAccessor mem;
    private CCR ccr;
    private boolean updateCCR;

    public Dex(Register pc, Register reg, CCR regCCR, boolean updateCCR) {
        super(pc, "DEX " + reg.getName(), reg.getName() + " - 1 -> " + reg.getName());
        this.reg = reg;
        this.ccr = regCCR;
        this.updateCCR = updateCCR;
    }

    @Override
    public int getCycles() {
        return 4;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int value = reg.getSignedValue() - 1;
        reg.setValue(value);

        if (updateCCR) {
            updateCCRZ(ccr, value, 2);
        }

        advancePC();
    }
}
