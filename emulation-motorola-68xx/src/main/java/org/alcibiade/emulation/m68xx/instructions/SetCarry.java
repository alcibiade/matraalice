package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class SetCarry extends AbstractInstruction {

    private CCR ccr;
    private boolean value;

    public SetCarry(Register pc, CCR regCCR, boolean value) {
        super(pc, value ? "SEC" : "CLC", "" + value + " -> C");
        this.ccr = regCCR;
        this.value = value;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        ccr.setC(value);
        advancePC();
    }
}
