package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryManager;

public class Psh extends AbstractInstruction {

    private Register valueRegister;
    private Register regSP;
    private MemoryManager memory;

    public Psh(Register pc, Register reg, Register sp, MemoryManager mem) {
        super(pc, "PSH " + reg.getName(), reg.getName() + " -> Stack");
        this.valueRegister = reg;
        this.regSP = sp;
        this.memory = mem;
    }

    @Override
    public int getCycles() {
        // TODO Check the duration for 16bit register push
        return 4;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int spOffset = regSP.getUnsignedValue();
        int value = valueRegister.getSignedValue();

        if (valueRegister.getSize() == 1) {
            memory.writeByte(spOffset, (byte) value);
            regSP.setValue(spOffset - 1);
        } else {
            memory.writeWord(spOffset - 1, (short) value);
            regSP.setValue(spOffset - 2);
        }

        advancePC();
    }
}
