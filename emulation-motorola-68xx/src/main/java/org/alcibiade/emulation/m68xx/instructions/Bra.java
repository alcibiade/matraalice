package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Bra extends BranchInstruction {

    public Bra(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "BRA", "");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return true;
    }
}
