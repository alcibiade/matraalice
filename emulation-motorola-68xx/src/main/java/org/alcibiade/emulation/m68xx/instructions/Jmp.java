package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Jmp extends BranchInstruction {

    public Jmp(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "JMP", "");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return true;
    }
}
