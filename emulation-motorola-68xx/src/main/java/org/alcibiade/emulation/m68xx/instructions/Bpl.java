package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Bpl extends BranchInstruction {

    public Bpl(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "BPL", "N = 0");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return !ccr.getN();
    }
}
