package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.DataAccessor;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Tst extends AbstractInstruction {

    private CCR ccr;
    private DataAccessor operand;

    public Tst(Register pc, MemoryAccessor mem, CCR ccr) {
        super(pc, "TST " + mem, "");
        this.ccr = ccr;
        this.operand = mem;
    }

    public Tst(Register pc, Register regOperand, CCR ccr) {
        super(pc, "TST " + regOperand.getName(), "");
        this.ccr = ccr;
        this.operand = regOperand;
    }

    @Override
    public int getCycles() {
        int result = 2;
        if (operand instanceof MemoryAccessor) {
            MemoryAccessor mem = (MemoryAccessor) operand;
            result = mem.getMode() == MemoryAccessMode.INDEXED ? 7 : 6;
        } else {
            result = 2;
        }

        return result;
    }

    @Override
    public int getSize() {
        int result = 2;
        if (operand instanceof MemoryAccessor) {
            MemoryAccessor mem = (MemoryAccessor) operand;
            result = mem.getMode() == MemoryAccessMode.INDEXED ? 2 : 3;
        } else {
            result = 1;
        }

        return result;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int value = operand.getUnsignedValue();
        updateCCRN(ccr, value, 1);
        updateCCRZ(ccr, value, 1);
        ccr.setV(false);
        ccr.setC(false);
        advancePC();
    }
}
