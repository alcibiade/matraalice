package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Blt extends BranchInstruction {

    public Blt(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "BLT", "N ^ V = 1");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return ccr.getN() ^ ccr.getV();
    }
}
