package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryManager;

public class Pul extends AbstractInstruction {

    private Register valueRegister;
    private Register regSP;
    private MemoryManager memory;

    public Pul(Register pc, Register reg, Register sp, MemoryManager mem) {
        super(pc, "PUL " + reg.getName(), "Stack -> " + reg.getName());
        this.valueRegister = reg;
        this.regSP = sp;
        this.memory = mem;
    }

    @Override
    public int getCycles() {
        // TODO Check the duration for 16bit register pull
        return 4;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int spOffset = regSP.getUnsignedValue() + valueRegister.getSize();
        int value;

        regSP.setValue(spOffset);

        if (valueRegister.getSize() == 1) {
            value = memory.readByte(spOffset);
        } else {
            value = memory.readWord(spOffset - 1);
        }

        valueRegister.setValue(value);

        advancePC();
    }
}
