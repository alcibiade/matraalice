package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Bmi extends BranchInstruction {

    public Bmi(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "BMI", "N = 1");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return ccr.getN();
    }
}
