package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Bge extends BranchInstruction {

    public Bge(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "BGE", "N ^ V = 0");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return !(ccr.getN() ^ ccr.getV());
    }
}
