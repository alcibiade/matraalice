package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Bvs extends BranchInstruction {

    public Bvs(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "BVS", "V = 1");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return ccr.getV();
    }
}
