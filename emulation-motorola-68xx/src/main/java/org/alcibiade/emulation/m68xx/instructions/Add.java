package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.DataAccessor;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Add extends AbstractInstruction {

    private CCR ccr;
    private DataAccessor operand;
    private Register reg;
    private boolean addCarry;

    public Add(Register pc, Register reg, MemoryAccessor mem, CCR ccr, boolean addCarry) {
        super(pc, "ADD " + reg.getName() + ", " + mem, reg.getName() + " + M -> " + reg.getName());
        this.ccr = ccr;
        this.reg = reg;
        this.operand = mem;
        this.addCarry = addCarry;
    }

    public Add(Register pc, Register reg, Register regOperand, CCR ccr) {
        super(pc, "ADD " + reg.getName() + ", " + regOperand.getName(),
                reg.getName() + " + " + regOperand.getName() + " -> " + reg.getName());
        this.ccr = ccr;
        this.reg = reg;
        this.operand = regOperand;
        this.addCarry = false;
    }

    @Override
    public int getCycles() {
        int result = 2;
        if (operand instanceof MemoryAccessor) {
            MemoryAccessor mem = (MemoryAccessor) operand;
            result = super.getCycles(mem.getMode());
        } else {
            result = 2;
        }

        return result;
    }

    @Override
    public int getSize() {
        int result = 2;
        if (operand instanceof MemoryAccessor) {
            MemoryAccessor mem = (MemoryAccessor) operand;
            result = super.getSize(mem.getMode());
        } else {
            result = 1;
        }

        return result;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int firstOp = reg.getUnsignedValue();
        int secondOp = operand.getUnsignedValue();
        int value = firstOp + secondOp;

        if (addCarry && ccr.getC()) {
            value += 1;
        }

        reg.setValue(value);

        updateCCRC(ccr, value, 1);
        updateCCRN(ccr, value, 1);
        updateCCRV8(ccr, firstOp, secondOp, value);
        updateCCRZ(ccr, value, 1);
        updateCCRH8(ccr, firstOp, secondOp, value);
        advancePC();
    }
}
