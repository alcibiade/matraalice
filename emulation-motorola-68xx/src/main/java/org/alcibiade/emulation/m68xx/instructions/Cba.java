package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Cba extends AbstractInstruction {

    private Register regA;
    private Register regB;
    private CCR ccr;

    public Cba(Register pc, Register regA, Register regB, CCR regCCR) {
        super(pc, "CBA", "A - B");
        this.regA = regA;
        this.regB = regB;
        this.ccr = regCCR;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int firstOp = regA.getUnsignedValue();
        int secondOp = regB.getUnsignedValue();
        int value = firstOp - secondOp;

        updateCCRN(ccr, value, 1);
        updateCCRZ(ccr, value, 1);
        updateCCRV8(ccr, firstOp, secondOp, value);
        updateCCRC(ccr, value, 1);

        advancePC();
    }
}
