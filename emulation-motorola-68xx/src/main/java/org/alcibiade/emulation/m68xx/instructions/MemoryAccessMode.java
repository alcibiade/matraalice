package org.alcibiade.emulation.m68xx.instructions;

public enum MemoryAccessMode {

    IMMEDIATE, DIRECT, INDEXED, EXTENDED, RELATIVE, INHERENT;
}
