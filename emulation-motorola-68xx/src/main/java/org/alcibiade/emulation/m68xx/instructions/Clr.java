package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.DataAccessor;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Clr extends AbstractInstruction {

    private DataAccessor mem;
    private CCR ccr;
    private int cycles;
    private int size;

    public Clr(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, "CLR " + mem, "00 -> M");
        this.mem = mem;
        this.ccr = regCCR;
        this.cycles = 2 + getCycles(mem.getMode());
        this.size = getSize(mem.getMode());
    }

    public Clr(Register pc, Register reg, CCR regCCR) {
        super(pc, "CLR " + reg.getName(), "00 -> " + reg.getName());
        this.mem = reg;
        this.ccr = regCCR;
        this.cycles = 2;
        this.size = 1;
    }

    @Override
    public int getCycles() {
        return cycles;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        mem.setValue(0);
        ccr.setN(false);
        ccr.setV(false);
        ccr.setC(false);
        ccr.setZ(true);
        advancePC();
    }
}
