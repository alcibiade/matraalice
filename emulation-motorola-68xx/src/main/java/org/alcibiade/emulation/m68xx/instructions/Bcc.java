package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Bcc extends BranchInstruction {

    public Bcc(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "BCC", "C = 0");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return !ccr.getC();
    }
}
