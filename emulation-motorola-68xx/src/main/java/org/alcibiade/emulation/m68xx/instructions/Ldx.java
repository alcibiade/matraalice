package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Ldx extends AbstractInstruction {

    private Register reg;
    private MemoryAccessor mem;
    private CCR ccr;

    public Ldx(Register pc, Register reg, MemoryAccessor mem, CCR regCCR) {
        super(pc, "LDX " + reg.getName() + ", " + mem, "M -> " + reg.getName());
        this.reg = reg;
        this.mem = mem;
        this.ccr = regCCR;
    }

    @Override
    public int getCycles() {
        return 1 + getCycles(mem.getMode());
    }

    @Override
    public int getSize() {
        return getSize16(mem.getMode());
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int value = mem.getSignedValue();
        reg.setValue(value);
        updateCCRN(ccr, value, 2);
        updateCCRZ(ccr, value, 2);
        ccr.setV(false);
        advancePC();
    }
}
