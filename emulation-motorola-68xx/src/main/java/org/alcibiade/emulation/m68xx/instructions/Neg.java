package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.DataAccessor;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Neg extends AbstractInstruction {

    private CCR ccr;
    private DataAccessor operand;
    private int size;
    private int cycles;

    public Neg(Register pc, MemoryAccessor operand, CCR ccr) {
        super(pc, "NEG " + operand.getName(), "M - 1 -> M");
        this.ccr = ccr;
        this.operand = operand;
        this.cycles = 2 + super.getCycles(operand.getMode());
        this.size = super.getSize(operand.getMode());
    }

    public Neg(Register pc, Register operand, CCR ccr) {
        super(pc, "NEG " + operand.getName(), operand.getName() + " - 1 -> " + operand.getName());
        assert operand.getSize() == 1;
        this.ccr = ccr;
        this.operand = operand;
        this.cycles = 2;
        this.size = 1;
    }

    @Override
    public int getCycles() {
        return cycles;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int valuePre = operand.getUnsignedValue();
        int value = -valuePre;
        operand.setValue(value);
        updateCCRN(ccr, value, 1);
        updateCCRZ(ccr, value, 1);
        ccr.setV(valuePre == 0x80);
        ccr.setC(valuePre == 0);
        advancePC();
    }
}
