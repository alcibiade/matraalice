package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Cpx extends AbstractInstruction {

    private Register reg;
    private MemoryAccessor mem;
    private CCR ccr;

    public Cpx(Register pc, Register reg, MemoryAccessor mem, CCR regCCR) {
        super(pc, "CPX " + reg.getName() + ", " + mem, reg.getName() + " - M");
        this.reg = reg;
        this.mem = mem;
        this.ccr = regCCR;
    }

    @Override
    public int getCycles() {
        return 1 + getCycles(mem.getMode());
    }

    @Override
    public int getSize() {
        return getSize16(mem.getMode());
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int firstOp = reg.getUnsignedValue();
        int secondOp = mem.getUnsignedValue();
        int value = firstOp - secondOp;

        updateCCRN(ccr, value, 2);
        updateCCRZ(ccr, value, 2);
        updateCCRV16(ccr, firstOp, secondOp, value);

        advancePC();
    }
}
