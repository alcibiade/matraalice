package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

/**
 * BIT updates the N and Z flags like an AND but without updating operands.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
public class Bit extends AbstractInstruction {

    private Register reg;
    private MemoryAccessor mem;
    private CCR ccr;

    public Bit(Register pc, Register reg, MemoryAccessor mem, CCR regCCR) {
        super(pc, "BIT " + reg.getName() + ", " + mem, reg.getName() + " . M");
        this.reg = reg;
        this.mem = mem;
        this.ccr = regCCR;
    }

    @Override
    public int getCycles() {
        return getCycles(mem.getMode());
    }

    @Override
    public int getSize() {
        return getSize(mem.getMode());
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int regValue = reg.getSignedValue();
        int memValue = mem.getSignedValue();
        int value = (byte) (regValue & memValue);
        updateCCRN(ccr, value, 1);
        updateCCRZ(ccr, value, 1);
        ccr.setV(false);
        advancePC();
    }
}
