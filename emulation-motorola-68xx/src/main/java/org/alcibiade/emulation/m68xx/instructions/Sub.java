package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.DataAccessor;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Sub extends AbstractInstruction {

    private CCR ccr;
    private DataAccessor operand;
    private Register reg;

    public Sub(Register pc, Register reg, MemoryAccessor mem, CCR ccr) {
        super(pc, "SUB " + reg.getName() + ", " + mem, reg.getName() + " - M -> " + reg.getName());
        this.ccr = ccr;
        this.reg = reg;
        this.operand = mem;
    }

    public Sub(Register pc, Register reg, Register reg2, CCR ccr) {
        super(pc, "SUB " + reg.getName() + ", " + reg2.getName(), reg.getName() + " - " + reg2.
                getName()
                + " -> " + reg.getName());
        this.ccr = ccr;
        this.reg = reg;
        this.operand = reg2;
    }

    @Override
    public int getCycles() {
        int result = 2;
        if (operand instanceof MemoryAccessor) {
            MemoryAccessor mem = (MemoryAccessor) operand;
            result = super.getCycles(mem.getMode());
        } else {
            result = 2;
        }

        return result;
    }

    @Override
    public int getSize() {
        int result = 2;
        if (operand instanceof MemoryAccessor) {
            MemoryAccessor mem = (MemoryAccessor) operand;
            result = super.getSize(mem.getMode());
        } else {
            result = 1;
        }

        return result;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int firstOp = reg.getUnsignedValue();
        int secondOp = operand.getUnsignedValue();
        int value = firstOp - secondOp;
        reg.setValue(value);
        updateCCRC(ccr, value, 1);
        updateCCRN(ccr, value, 1);
        updateCCRV8(ccr, firstOp, secondOp, value);
        updateCCRZ(ccr, value, 1);
        advancePC();
    }
}
