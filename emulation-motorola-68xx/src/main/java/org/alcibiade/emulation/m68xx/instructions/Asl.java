package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.DataAccessor;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Asl extends AbstractInstruction {

    private DataAccessor data;
    private CCR ccr;
    private int cycles;
    private int size;
    private int bytes;

    public Asl(Register pc, Register data, CCR regCCR) {
        super(pc, "ASL " + data.getName(), "");
        this.data = data;
        this.bytes = data.getSize();
        this.cycles = bytes == 1 ? 2 : 4;
        this.size = 1;
        this.ccr = regCCR;
    }

    public Asl(Register pc, MemoryAccessor data, CCR regCCR) {
        super(pc, "ASL " + data, "");
        this.data = data;
        this.bytes = 1;
        this.cycles = 2 + getCycles(data.getMode());
        this.size = getSize(data.getMode());
        this.ccr = regCCR;
    }

    @Override
    public int getCycles() {
        return cycles;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int value = data.getUnsignedValue();
        value = value << 1;

        data.setValue(value);

        updateCCRN(ccr, value, bytes);
        updateCCRZ(ccr, value, bytes);
        updateCCRC(ccr, value, bytes);
        ccr.setV(ccr.getN() != ccr.getC());
        advancePC();
    }
}
