package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Bcs extends BranchInstruction {

    public Bcs(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "BCS", "C = 1");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return ccr.getC();
    }
}
