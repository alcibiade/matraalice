package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.cpu.Register;

public abstract class AbstractInstruction implements Instruction {

    private int size;
    private int cycles;
    private String assembly;
    private String notes;
    private Register regPC;

    public AbstractInstruction(Register regPC, String assembly, String notes) {
        this.size = 1;
        this.cycles = 2;
        this.assembly = assembly;
        this.notes = notes;
        this.regPC = regPC;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public int getCycles() {
        return cycles;
    }

    @Override
    public String getAssembly() {
        return assembly;
    }

    @Override
    public String getNotes() {
        return notes;
    }

    protected void advancePC() {
        advancePC(regPC.getSignedValue() + getSize());
    }

    protected void advancePC(int address) {
        regPC.setValue(address);
    }

    protected int getCycles(MemoryAccessMode memoryAccessMode) {
        int result;

        switch (memoryAccessMode) {
            case DIRECT:
                result = 3;
                break;
            case INDEXED:
                result = 5;
                break;
            case EXTENDED:
                result = 4;
                break;
            default:
                result = 2;
        }

        return result;
    }

    protected int getSize(MemoryAccessMode memoryAccessMode) {
        int result;

        switch (memoryAccessMode) {
            case EXTENDED:
                result = 3;
                break;

            default:
                result = 2;
        }

        return result;
    }

    protected int getSize16(MemoryAccessMode memoryAccessMode) {
        int result;

        switch (memoryAccessMode) {
            case EXTENDED:
            case IMMEDIATE:
                result = 3;
                break;

            default:
                result = 2;
        }

        return result;
    }

    protected void updateCCRN(CCR ccr, int value, int bytes) {
        int mask = bytes == 1 ? 0x80 : 0x8000;
        ccr.setN((value & mask) != 0);
    }

    protected void updateCCRZ(CCR ccr, int value, int bytes) {
        int mask = bytes == 1 ? 0xFF : 0xFFFF;
        ccr.setZ((value & mask) == 0);
    }

    protected void updateCCRH8(CCR ccr, int first, int second, int result) {
        assert first == (first & 0xFF);
        assert second == (second & 0xFF);
        int flag = ((first ^ second ^ result) & 0x10) >> 3;
        ccr.setH(flag != 0);
    }

    protected void updateCCRV8(CCR ccr, int first, int second, int result) {
        assert first == (first & 0xFF);
        assert second == (second & 0xFF);
        int flag = ((first ^ second ^ result ^ (result >> 1)) & 0x80) >> 7;
        ccr.setV(flag != 0);
    }

    protected void updateCCRV16(CCR ccr, int first, int second, int result) {
        assert first == (first & 0xFFFF);
        assert second == (second & 0xFFFF);
        int flag = ((first ^ second ^ result ^ (result >> 1)) & 0x8000) >> 15;
        ccr.setV(flag != 0);
    }

    protected void updateCCRC(CCR ccr, int value, int bytes) {
        int mask = bytes == 1 ? 0xFF00 : 0xFFFF0000;
        ccr.setC((value & mask) != 0);
    }
}
