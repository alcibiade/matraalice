package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.MemoryManager;

public class Jsr extends BranchSubInstruction {

    public Jsr(Register pc, MemoryAccessor parameter, CCR regCCR, Register regSP,
            MemoryManager memory) {
        super(pc, parameter, regCCR, "JSR " + parameter, "", regSP, memory);
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return true;
    }
}
