package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.StaticRegister;

public class CCR extends StaticRegister {

    // Carry from bit 7
    public static final int MASK_C = 0x01;
    // Overflow, 2's complement
    public static final int MASK_V = 0x01 << 1;
    // Zero (byte)
    public static final int MASK_Z = 0x01 << 2;
    // Negative (sign bit)
    public static final int MASK_N = 0x01 << 3;
    // Interrupt mask
    public static final int MASK_I = 0x01 << 4;
    // Half-carry from bit 3
    public static final int MASK_H = 0x01 << 5;

    public CCR() {
        super("CCR", 8);
    }

    public boolean getZ() {
        return get(MASK_Z);
    }

    public boolean getC() {
        return get(MASK_C);
    }

    public boolean getN() {
        return get(MASK_N);
    }

    public boolean getV() {
        return get(MASK_V);
    }

    public boolean getI() {
        return get(MASK_I);
    }

    public boolean getH() {
        return get(MASK_H);
    }

    public void setC(boolean value) {
        set(MASK_C, value);
    }

    public void setV(boolean value) {
        set(MASK_V, value);
    }

    public void setZ(boolean value) {
        set(MASK_Z, value);
    }

    public void setN(boolean value) {
        set(MASK_N, value);
    }

    public void setI(boolean value) {
        set(MASK_I, value);
    }

    public void setH(boolean value) {
        set(MASK_H, value);
    }

    private void set(int mask, boolean value) {
        if (value) {
            setValue(getSignedValue() | mask);
        } else {
            setValue(getSignedValue() & (~mask));
        }
    }

    private boolean get(int mask) {
        return (getSignedValue() & mask) != 0;
    }

    @Override
    public String toString() {
        String text = Integer.toBinaryString(getSignedValue());

        while (text.length() < 8) {
            text = "0" + text;
        }

        return text + "b";
    }
}
