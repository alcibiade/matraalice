package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Tba extends AbstractInstruction {

    private Register regA;
    private Register regB;
    private CCR ccr;

    public Tba(Register pc, Register regA, Register regB, CCR regCCR) {
        super(pc, "TBA", "B -> A");
        this.regA = regA;
        this.regB = regB;
        this.ccr = regCCR;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int value = regB.getUnsignedValue();
        regA.setValue(value);

        updateCCRN(ccr, value, 1);
        updateCCRZ(ccr, value, 1);
        ccr.setV(false);

        advancePC();
    }
}
