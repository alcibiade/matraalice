package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class ComRegister extends AbstractInstruction {

    private Register reg;
    private CCR ccr;

    public ComRegister(Register pc, Register reg, CCR regCCR) {
        super(pc, "COM " + reg.getName(), "~" + reg.getName() + " -> " + reg.getName());
        this.reg = reg;
        this.ccr = regCCR;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int value = ~reg.getSignedValue();
        reg.setValue(value);
        updateCCRN(ccr, value, 1);
        updateCCRZ(ccr, value, 1);
        ccr.setV(false);
        ccr.setC(true);
        advancePC();
    }
}
