package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Addd extends AbstractInstruction {

    private CCR ccr;
    private MemoryAccessor mem;
    private Register reg;

    public Addd(Register pc, Register reg, MemoryAccessor mem, CCR ccr) {
        super(pc, "ADDD " + reg.getName() + ", " + mem, reg.getName() + " - M -> " + reg.getName());
        this.ccr = ccr;
        this.reg = reg;
        this.mem = mem;
    }

    @Override
    public int getCycles() {
        return 2 + super.getCycles(mem.getMode());
    }

    @Override
    public int getSize() {
        return super.getSize16(mem.getMode());
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int firstOp = reg.getUnsignedValue();
        int secondOp = mem.getUnsignedValue();
        int value = firstOp + secondOp;
        reg.setValue(value);
        updateCCRC(ccr, value, 2);
        updateCCRN(ccr, value, 2);
        updateCCRV16(ccr, firstOp, secondOp, value);
        updateCCRZ(ccr, value, 2);
        advancePC();
    }
}
