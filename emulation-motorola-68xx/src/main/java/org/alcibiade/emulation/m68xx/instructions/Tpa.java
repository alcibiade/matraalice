package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Tpa extends AbstractInstruction {

    private Register reg;
    private CCR ccr;

    public Tpa(Register pc, Register reg, CCR regCCR) {
        super(pc, "TAP", "CCR -> A");
        this.reg = reg;
        this.ccr = regCCR;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        reg.setValue(ccr.getUnsignedValue());
        advancePC();
    }
}
