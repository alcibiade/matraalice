package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Tab extends AbstractInstruction {

    private Register regA;
    private Register regB;
    private CCR ccr;

    public Tab(Register pc, Register regA, Register regB, CCR regCCR) {
        super(pc, "TAB", "A -> B");
        this.regA = regA;
        this.regB = regB;
        this.ccr = regCCR;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int value = regA.getUnsignedValue();
        regB.setValue(value);

        updateCCRN(ccr, value, 1);
        updateCCRZ(ccr, value, 1);
        ccr.setV(false);

        advancePC();
    }
}
