package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.DataAccessor;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Ror extends AbstractInstruction {

    private DataAccessor data;
    private CCR ccr;
    private int cycles;
    private int size;

    public Ror(Register pc, Register data, CCR regCCR) {
        super(pc, "ROR " + data.getName(), "");
        this.data = data;
        this.cycles = 2;
        this.size = 1;
        this.ccr = regCCR;
    }

    public Ror(Register pc, MemoryAccessor data, CCR regCCR) {
        super(pc, "ROR " + data, "");
        this.data = data;
        this.cycles = 2 + getCycles(data.getMode());
        this.size = getSize(data.getMode());
        this.ccr = regCCR;
    }

    @Override
    public int getCycles() {
        return cycles;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int value = data.getUnsignedValue();
        boolean c = ccr.getC();
        ccr.setC((value & 0x01) != 0);

        value = value >> 1;
        if (c) {
            value |= 0x80;
        }

        data.setValue(value);

        updateCCRN(ccr, value, 1);
        updateCCRZ(ccr, value, 1);
        ccr.setV(ccr.getN() ^ ccr.getC());
        advancePC();
    }
}
