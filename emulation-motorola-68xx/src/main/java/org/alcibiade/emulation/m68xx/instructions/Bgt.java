package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Bgt extends BranchInstruction {

    public Bgt(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "BGT", "Z | (N ^ V) = 0");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return !(ccr.getZ() || (ccr.getN() ^ ccr.getV()));
    }
}
