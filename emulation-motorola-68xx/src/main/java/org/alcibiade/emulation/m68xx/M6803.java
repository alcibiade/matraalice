package org.alcibiade.emulation.m68xx;

import javax.annotation.PostConstruct;
import org.alcibiade.emulation.core.cpu.AbstractCpu;
import org.alcibiade.emulation.core.cpu.BinUtils;
import org.alcibiade.emulation.core.memory.BigEndianMemoryManager;
import org.alcibiade.emulation.core.cpu.CompositeRegister;
import org.alcibiade.emulation.core.cpu.IllegalInstructionCodeException;
import org.alcibiade.emulation.core.cpu.Instruction;
import org.alcibiade.emulation.core.memory.MemoryManager;
import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.cpu.StaticRegister;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryUtils;
import org.alcibiade.emulation.m68xx.instructions.Abx;
import org.alcibiade.emulation.m68xx.instructions.Add;
import org.alcibiade.emulation.m68xx.instructions.Addd;
import org.alcibiade.emulation.m68xx.instructions.And;
import org.alcibiade.emulation.m68xx.instructions.Asl;
import org.alcibiade.emulation.m68xx.instructions.Asr;
import org.alcibiade.emulation.m68xx.instructions.Bcc;
import org.alcibiade.emulation.m68xx.instructions.Bcs;
import org.alcibiade.emulation.m68xx.instructions.Beq;
import org.alcibiade.emulation.m68xx.instructions.Bge;
import org.alcibiade.emulation.m68xx.instructions.Bgt;
import org.alcibiade.emulation.m68xx.instructions.Bhi;
import org.alcibiade.emulation.m68xx.instructions.Bit;
import org.alcibiade.emulation.m68xx.instructions.Bls;
import org.alcibiade.emulation.m68xx.instructions.Blt;
import org.alcibiade.emulation.m68xx.instructions.Bmi;
import org.alcibiade.emulation.m68xx.instructions.Bne;
import org.alcibiade.emulation.m68xx.instructions.Bpl;
import org.alcibiade.emulation.m68xx.instructions.Bra;
import org.alcibiade.emulation.m68xx.instructions.Bsr;
import org.alcibiade.emulation.m68xx.instructions.Bvc;
import org.alcibiade.emulation.m68xx.instructions.Bvs;
import org.alcibiade.emulation.m68xx.instructions.CCR;
import org.alcibiade.emulation.m68xx.instructions.Cba;
import org.alcibiade.emulation.m68xx.instructions.Clr;
import org.alcibiade.emulation.m68xx.instructions.Cmp;
import org.alcibiade.emulation.m68xx.instructions.ComMemory;
import org.alcibiade.emulation.m68xx.instructions.ComRegister;
import org.alcibiade.emulation.m68xx.instructions.Cpx;
import org.alcibiade.emulation.m68xx.instructions.Dec;
import org.alcibiade.emulation.m68xx.instructions.Dex;
import org.alcibiade.emulation.m68xx.instructions.Eor;
import org.alcibiade.emulation.m68xx.instructions.Inc;
import org.alcibiade.emulation.m68xx.instructions.Inx;
import org.alcibiade.emulation.m68xx.instructions.Jmp;
import org.alcibiade.emulation.m68xx.instructions.Jsr;
import org.alcibiade.emulation.m68xx.instructions.Lda;
import org.alcibiade.emulation.m68xx.instructions.Ldx;
import org.alcibiade.emulation.m68xx.instructions.Lsr;
import org.alcibiade.emulation.m68xx.instructions.MemoryAccessMode;
import org.alcibiade.emulation.m68xx.instructions.MemoryAccessor;
import org.alcibiade.emulation.m68xx.instructions.Neg;
import org.alcibiade.emulation.m68xx.instructions.Nop;
import org.alcibiade.emulation.m68xx.instructions.Or;
import org.alcibiade.emulation.m68xx.instructions.Pul;
import org.alcibiade.emulation.m68xx.instructions.Psh;
import org.alcibiade.emulation.m68xx.instructions.Rol;
import org.alcibiade.emulation.m68xx.instructions.Ror;
import org.alcibiade.emulation.m68xx.instructions.Rts;
import org.alcibiade.emulation.m68xx.instructions.SetCarry;
import org.alcibiade.emulation.m68xx.instructions.SetInterrupt;
import org.alcibiade.emulation.m68xx.instructions.SetOverflow;
import org.alcibiade.emulation.m68xx.instructions.Sta;
import org.alcibiade.emulation.m68xx.instructions.Stx;
import org.alcibiade.emulation.m68xx.instructions.Sub;
import org.alcibiade.emulation.m68xx.instructions.Subd;
import org.alcibiade.emulation.m68xx.instructions.Tab;
import org.alcibiade.emulation.m68xx.instructions.Tap;
import org.alcibiade.emulation.m68xx.instructions.Tba;
import org.alcibiade.emulation.m68xx.instructions.Tpa;
import org.alcibiade.emulation.m68xx.instructions.Tst;
import org.alcibiade.emulation.m68xx.instructions.Tsx;
import org.alcibiade.emulation.m68xx.instructions.Txs;

// CHECKSTYLE IGNORE ClassDataAbstractionCoupling FOR NEXT 1 LINES
public class M6803 extends AbstractCpu {

    private static final int VECTOR_RESET = 0xFFFE;
    private Register regA;
    private Register regB;
    private Register regD;
    private Register regX;
    private Register regPC;
    private Register regSP;
    private CCR regCCR;
    private MemoryManager memoryManager;

    public M6803() {
        regA = new StaticRegister("A", 8);
        regB = new StaticRegister("B", 8);
        regD = new CompositeRegister("D", regA, regB);

        regX = new StaticRegister("X", 16);
        regPC = new StaticRegister("PC", 16);
        regSP = new StaticRegister("SP", 16);
        regCCR = new CCR();
    }

    @PostConstruct
    public void init() throws IllegalMemoryAccessException {
        memoryManager = new BigEndianMemoryManager(getMemory());
        regPC.setValue(memoryManager.readWord(VECTOR_RESET));
    }

    protected Register getRegisterX() {
        return regX;
    }

    protected CCR getRegisterCCR() {
        return regCCR;
    }

    protected Register getRegisterSP() {
        return regSP;
    }

    @Override
    protected int getProgramAddress() {
        return regPC.getUnsignedValue();
    }

    // CHECKSTYLE IGNORE ExecutableStatementCountCheck FOR NEXT 12 LINES
    // CHECKSTYLE IGNORE CyclomaticComplexityCheck FOR NEXT 12 LINES
    // CHECKSTYLE IGNORE MethodLengthCheck FOR NEXT 12 LINES
    // CHECKSTYLE IGNORE JavaNCSSCheck FOR NEXT 12 LINES
    @Override
    protected Instruction parseInstructionAt(int offset)
            throws IllegalMemoryAccessException, IllegalInstructionCodeException {
        int opCode = BinUtils.convertToUnsigned(memoryManager.readByte(offset), 8);

        Instruction instruction = null;
        MemoryAccessMode accessMode = computeAccessMode(opCode);
        Register accumulator = computeAccumulator(opCode);
        MemoryAccessor memory = new MemoryAccessor(memoryManager, accessMode, regPC, regX, 1);
        MemoryAccessor memory16 = new MemoryAccessor(memoryManager, accessMode, regPC, regX, 2);

        switch (opCode) {
            case 0x01:
                instruction = new Nop(regPC);
                break;

            case 0x06:
                instruction = new Tap(regPC, regA, regCCR);
                break;

            case 0x07:
                instruction = new Tpa(regPC, regA, regCCR);
                break;

            case 0x08:
                instruction = new Inx(regPC, regX, regCCR, true);
                break;

            case 0x31:
                instruction = new Inx(regPC, regSP, regCCR, false);
                break;

            case 0x09:
                instruction = new Dex(regPC, regX, regCCR, true);
                break;

            case 0x34:
                instruction = new Dex(regPC, regSP, regCCR, false);
                break;

            case 0x0A:
            case 0x0B:
                instruction = new SetOverflow(regPC, regCCR, opCode == 0x0B);
                break;

            case 0x0C:
            case 0x0D:
                instruction = new SetCarry(regPC, regCCR, opCode == 0x0D);
                break;

            case 0x0E:
            case 0x0F:
                instruction = new SetInterrupt(regPC, regCCR, opCode == 0x0F);
                break;

            case 0x11:
                instruction = new Cba(regPC, regA, regB, regCCR);
                break;

            case 0x16:
                instruction = new Tab(regPC, regA, regB, regCCR);
                break;

            case 0x17:
                instruction = new Tba(regPC, regA, regB, regCCR);
                break;

            case 0x20:
                instruction = new Bra(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x22:
                instruction = new Bhi(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x23:
                instruction = new Bls(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x24:
                instruction = new Bcc(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x25:
                instruction = new Bcs(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x26:
                instruction = new Bne(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x27:
                instruction = new Beq(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x28:
                instruction = new Bvc(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x29:
                instruction = new Bvs(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x2A:
                instruction = new Bpl(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x2B:
                instruction = new Bmi(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x2C:
                instruction = new Bge(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x2D:
                instruction = new Blt(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x2E:
                instruction = new Bgt(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x2F:
                instruction = new Blt(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR);
                break;

            case 0x6E:
                instruction = new Jmp(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.INDEXED, regPC, regX, 1), regCCR);
                break;

            case 0x7E:
                instruction = new Jmp(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.EXTENDED, regPC, regX, 1), regCCR);
                break;

            case 0x32:
            case 0x33:
                instruction = new Pul(regPC, accumulator, regSP, memoryManager);
                break;

            case 0x36:
            case 0x37:
                instruction = new Psh(regPC, accumulator, regSP, memoryManager);
                break;

            case 0x38:
                instruction = new Pul(regPC, regX, regSP, memoryManager);
                break;

            case 0x3C:
                instruction = new Psh(regPC, regX, regSP, memoryManager);
                break;

            case 0xAD:
            case 0xBD:
                instruction = new Jsr(regPC, memory16, regCCR, regSP, memoryManager);
                break;

            case 0x8D:
                instruction = new Bsr(regPC, new MemoryAccessor(memoryManager,
                        MemoryAccessMode.RELATIVE, regPC, regX, 1), regCCR, regSP, memoryManager);
                break;

            case 0x39:
                instruction = new Rts(regPC, regSP, memoryManager);
                break;

            case 0x6F:
            case 0x7F:
                instruction = new Clr(regPC, memory, regCCR);
                break;

            case 0x4F:
            case 0x5F:
                instruction = new Clr(regPC, accumulator, regCCR);
                break;

            case 0x63:
            case 0x73:
                instruction = new ComMemory(regPC, memory, regCCR);
                break;

            case 0x43:
            case 0x53:
                instruction = new ComRegister(regPC, accumulator, regCCR);
                break;

            case 0x44:
            case 0x54:
                instruction = new Lsr(regPC, accumulator, regCCR);
                break;

            case 0x64:
            case 0x74:
                instruction = new Lsr(regPC, memory, regCCR);
                break;

            case 0x46:
            case 0x56:
                instruction = new Ror(regPC, accumulator, regCCR);
                break;

            case 0x66:
            case 0x76:
                instruction = new Ror(regPC, memory, regCCR);
                break;

            case 0x47:
            case 0x57:
                instruction = new Asr(regPC, accumulator, regCCR);
                break;

            case 0x67:
            case 0x77:
                instruction = new Asr(regPC, memory, regCCR);
                break;

            case 0x05:
                instruction = new Asl(regPC, regD, regCCR);
                break;

            case 0x48:
            case 0x58:
                instruction = new Asl(regPC, accumulator, regCCR);
                break;

            case 0x68:
            case 0x78:
                instruction = new Asl(regPC, memory, regCCR);
                break;

            case 0x49:
            case 0x59:
                instruction = new Rol(regPC, accumulator, regCCR);
                break;

            case 0x69:
            case 0x79:
                instruction = new Rol(regPC, memory, regCCR);
                break;

            case 0x4A:
            case 0x5A:
                instruction = new Dec(regPC, accumulator, regCCR);
                break;

            case 0x6A:
            case 0x7A:
                instruction = new Dec(regPC, memory, regCCR);
                break;

            case 0x4C:
            case 0x5C:
                instruction = new Inc(regPC, accumulator, regCCR);
                break;

            case 0x6C:
            case 0x7C:
                instruction = new Inc(regPC, memory, regCCR);
                break;

            case 0x40:
            case 0x50:
                instruction = new Neg(regPC, accumulator, regCCR);
                break;

            case 0x60:
            case 0x70:
                instruction = new Neg(regPC, memory, regCCR);
                break;

            case 0x8B:
            case 0x9B:
            case 0xAB:
            case 0xBB:

            case 0xCB:
            case 0xDB:
            case 0xEB:
            case 0xFB:
                instruction = new Add(regPC, accumulator, memory, regCCR, false);
                break;

            case 0x89:
            case 0x99:
            case 0xA9:
            case 0xB9:

            case 0xC9:
            case 0xD9:
            case 0xE9:
            case 0xF9:
                instruction = new Add(regPC, accumulator, memory, regCCR, true);
                break;

            case 0x88:
            case 0x98:
            case 0xA8:
            case 0xB8:

            case 0xC8:
            case 0xD8:
            case 0xE8:
            case 0xF8:
                instruction = new Eor(regPC, accumulator, memory, regCCR);
                break;

            case 0x84:
            case 0x94:
            case 0xA4:
            case 0xB4:

            case 0xC4:
            case 0xD4:
            case 0xE4:
            case 0xF4:
                instruction = new And(regPC, accumulator, memory, regCCR);
                break;

            case 0x85:
            case 0x95:
            case 0xA5:
            case 0xB5:

            case 0xC5:
            case 0xD5:
            case 0xE5:
            case 0xF5:
                instruction = new Bit(regPC, accumulator, memory, regCCR);
                break;

            case 0x8A:
            case 0x9A:
            case 0xAA:
            case 0xBA:

            case 0xCA:
            case 0xDA:
            case 0xEA:
            case 0xFA:
                instruction = new Or(regPC, accumulator, memory, regCCR);
                break;

            case 0x10:
                instruction = new Sub(regPC, regA, regB, regCCR);
                break;

            case 0x1B:
                instruction = new Add(regPC, regA, regB, regCCR);
                break;

            case 0xC3:
            case 0xD3:
            case 0xE3:
            case 0xF3:
                instruction = new Addd(regPC, regD, memory16, regCCR);
                break;

            case 0x3A:
                instruction = new Abx(regPC, regX, regB);
                break;

            case 0x80:
            case 0x90:
            case 0xA0:
            case 0xB0:

            case 0xC0:
            case 0xD0:
            case 0xE0:
            case 0xF0:
                instruction = new Sub(regPC, accumulator, memory, regCCR);
                break;

            case 0x83:
            case 0x93:
            case 0xA3:
            case 0xB3:
                instruction = new Subd(regPC, regD, memory16, regCCR);
                break;

            case 0x86:
            case 0x96:
            case 0xA6:
            case 0xB6:

            case 0xC6:
            case 0xD6:
            case 0xE6:
            case 0xF6:
                instruction = new Lda(regPC, accumulator, memory, regCCR);
                break;

            case 0xCE:
            case 0xDE:
            case 0xEE:
            case 0xFE:
                instruction = new Ldx(regPC, regX, memory16, regCCR);
                break;

            case 0xCC:
            case 0xDC:
            case 0xEC:
            case 0xFC:
                instruction = new Ldx(regPC, regD, memory16, regCCR);
                break;

            case 0x8E:
            case 0x9E:
            case 0xAE:
            case 0xBE:
                instruction = new Ldx(regPC, regSP, memory16, regCCR);
                break;

            case 0x97:
            case 0xA7:
            case 0xB7:

            case 0xD7:
            case 0xE7:
            case 0xF7:
                instruction = new Sta(regPC, accumulator, memory, regCCR);
                break;

            case 0x9F:
            case 0xAF:
            case 0xBF:
                instruction = new Stx(regPC, regSP, memory16, regCCR);
                break;

            case 0xDF:
            case 0xEF:
            case 0xFF:
                instruction = new Stx(regPC, regX, memory16, regCCR);
                break;

            case 0xDD:
            case 0xED:
            case 0xFD:
                instruction = new Stx(regPC, regD, memory16, regCCR);
                break;

            case 0x81:
            case 0x91:
            case 0xA1:
            case 0xB1:

            case 0xC1:
            case 0xD1:
            case 0xE1:
            case 0xF1:
                instruction = new Cmp(regPC, accumulator, memory, regCCR);
                break;

            case 0x8C:
            case 0x9C:
            case 0xAC:
            case 0xBC:
                instruction = new Cpx(regPC, regX, memory16, regCCR);
                break;

            case 0x4D:
            case 0x5D:
                instruction = new Tst(regPC, accumulator, regCCR);
                break;

            case 0x6D:
            case 0x7D:
                instruction = new Tst(regPC, memory, regCCR);
                break;

            case 0x30:
                instruction = new Tsx(regPC, regX, regSP, regCCR);
                break;

            case 0x35:
                instruction = new Txs(regPC, regX, regSP, regCCR);
                break;
        }

        if (instruction == null) {
            throw new IllegalInstructionCodeException(offset, opCode);
        }

        return instruction;
    }

    protected static MemoryAccessMode computeAccessMode(int opCode) {
        MemoryAccessMode accessMode;
        int code = opCode & 0x30;

        if (opCode == 0x8D) {
            accessMode = MemoryAccessMode.RELATIVE;
        } else if (opCode < 0x60) {
            if (code == 0x20) {
                accessMode = MemoryAccessMode.RELATIVE;
            } else {
                accessMode = MemoryAccessMode.INHERENT;
            }
        } else {
            if (code == 0x00) {
                accessMode = MemoryAccessMode.IMMEDIATE;
            } else if (code == 0x10) {
                accessMode = MemoryAccessMode.DIRECT;
            } else if (code == 0x20) {
                accessMode = MemoryAccessMode.INDEXED;
            } else {
                accessMode = MemoryAccessMode.EXTENDED;
            }
        }

        return accessMode;
    }

    protected Register computeAccumulator(int opCode) {
        Register result = null;

        if ((opCode & 0xF0) == 0x30) {
            result = (opCode & 0x01) == 0x00 ? regA : regB;
        } else if ((opCode & 0xF0) >= 0x80) {
            result = (opCode & 0x40) == 0x00 ? regA : regB;
        } else {
            result = (opCode & 0x50) == 0x40 ? regA : regB;
        }

        return result;
    }

    @Override
    public String getDebugInformations() {
        StringBuilder text = new StringBuilder();
        text.append("=== M6803 ===================\n");
        text.append("|   A: " + regA + "            |\n");
        text.append("|   B: " + regB + "            |\n");
        text.append("|   D: " + regD + "  |\n");
        text.append("|   X: " + regX + "            |\n");
        text.append("|  PC: " + regPC + "            |\n");
        text.append("|  SP: " + regSP + "            |\n");
        text.append("| CCR: " + regCCR + "            |\n");
        text.append("|      ..HINZVC             |\n");
        text.append("=== INTERNAL MEMORY =================================================\n");
        text.append(MemoryUtils.dump(getMemory(), 128, 128));
        text.append("=== KEYBOARD BUFFER =================================================\n");
        text.append(MemoryUtils.dump(getMemory(), 0x42B2, 16));
        text.append("=== CODE ============================================================\n");
        text.append(MemoryUtils.dump(getMemory(), regPC.getUnsignedValue() - 16, 32));
        text.append("=== STACK ===========================================================\n");
        text.append(MemoryUtils.dump(getMemory(), regSP.getUnsignedValue() - 16, 32));
        text.append("=== DATA ============================================================\n");
        text.append(MemoryUtils.dump(getMemory(), regX.getUnsignedValue() - 16, 48));
        text.append("=====================================================================\n");
        return text.toString();
    }
}
