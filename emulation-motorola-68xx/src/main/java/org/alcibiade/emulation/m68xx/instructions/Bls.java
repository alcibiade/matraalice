package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Bls extends BranchInstruction {

    public Bls(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "BLS", "C | Z = 1");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return ccr.getC() || ccr.getZ();
    }
}
