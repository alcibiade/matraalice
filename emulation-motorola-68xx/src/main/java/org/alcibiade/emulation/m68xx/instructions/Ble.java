package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Ble extends BranchInstruction {

    public Ble(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "BLE", "Z | (N ^ V) = 1");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return ccr.getZ() || (ccr.getN() ^ ccr.getV());
    }
}
