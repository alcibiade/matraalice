package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Nop extends AbstractInstruction {

    public Nop(Register pc) {
        super(pc, "NOP", "");
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        advancePC();
    }
}
