package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Abx extends AbstractInstruction {

    private Register regX;
    private Register regB;
    private boolean addCarry;

    public Abx(Register pc, Register regX, Register regB) {
        super(pc, "ABX", "X + B -> X");
        this.regX = regX;
        this.regB = regB;
    }

    @Override
    public int getCycles() {
        return 3;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        regX.setValue(regX.getSignedValue() + regB.getSignedValue());
        advancePC();
    }
}
