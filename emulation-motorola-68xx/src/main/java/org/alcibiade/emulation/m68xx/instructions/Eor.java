package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class Eor extends AbstractInstruction {

    private Register reg;
    private MemoryAccessor mem;
    private CCR ccr;

    public Eor(Register pc, Register reg, MemoryAccessor mem, CCR regCCR) {
        super(pc, "EOR " + reg.getName() + ", " + mem, reg.getName() + " x M -> " + reg.getName());
        this.reg = reg;
        this.mem = mem;
        this.ccr = regCCR;
    }

    @Override
    public int getCycles() {
        return getCycles(mem.getMode());
    }

    @Override
    public int getSize() {
        return getSize(mem.getMode());
    }

    @Override
    public void execute() throws IllegalMemoryAccessException {
        int value = reg.getSignedValue() ^ mem.getSignedValue();
        reg.setValue(value);
        updateCCRN(ccr, value, 1);
        updateCCRZ(ccr, value, 1);
        ccr.setV(false);
        advancePC();
    }
}
