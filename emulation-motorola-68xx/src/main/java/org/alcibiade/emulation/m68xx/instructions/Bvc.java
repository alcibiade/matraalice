package org.alcibiade.emulation.m68xx.instructions;

import org.alcibiade.emulation.core.cpu.Register;

public class Bvc extends BranchInstruction {

    public Bvc(Register pc, MemoryAccessor mem, CCR regCCR) {
        super(pc, mem, regCCR, "BVC", "V = 0");
    }

    @Override
    protected boolean decideBranch(CCR ccr) {
        return !ccr.getV();
    }
}
