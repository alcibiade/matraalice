package org.alcibiade.emulation.core.memory;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class BigEndianMemoryManagerTest {

    @Test
    public void testByteOperation() throws IllegalMemoryAccessException {
        RandomAccessible memory = new MemoryArea(256);
        MemoryManager manager = new BigEndianMemoryManager(memory);

        manager.writeByte(0x20, (byte) 0x5F);
        assertEquals((byte) 0x5F, memory.getByte(0x20));
        assertEquals((byte) 0x5F, manager.readByte(0x20));

        manager.writeByte(0x20, (byte) 0xFE);
        assertEquals((byte) 0xFE, memory.getByte(0x20));
        assertEquals((byte) 0xFE, manager.readByte(0x20));
    }

    @Test
    public void testWordOperation() throws IllegalMemoryAccessException {
        RandomAccessible memory = new MemoryArea(256);
        MemoryManager manager = new BigEndianMemoryManager(memory);

        manager.writeWord(0x20, (short) 0x6D5F);
        assertEquals((byte) 0x6D, memory.getByte(0x20));
        assertEquals((byte) 0x5F, memory.getByte(0x21));
        assertEquals((short) 0x6D5F, manager.readWord(0x20));

        manager.writeWord(0x20, (short) 0xFFFE);
        assertEquals((byte) 0xFF, memory.getByte(0x20));
        assertEquals((byte) 0xFE, memory.getByte(0x21));
        assertEquals((short) 0xFFFE, manager.readWord(0x20));

        manager.writeWord(0x20, (short) 0xABCD);
        assertEquals((byte) 0xAB, memory.getByte(0x20));
        assertEquals((byte) 0xCD, memory.getByte(0x21));
        assertEquals((short) 0xABCD, manager.readWord(0x20));
    }
}
