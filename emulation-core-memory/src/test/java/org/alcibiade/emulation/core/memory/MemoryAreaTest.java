package org.alcibiade.emulation.core.memory;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class MemoryAreaTest {

    @Test
    public void testReadWrite() throws IllegalMemoryAccessException {
        MemoryArea memory = new MemoryArea(32);

        for (int offs = 0; offs < 32; offs++) {
            memory.setByte(offs, (byte) offs);
        }

        for (int offs = 0; offs < 32; offs++) {
            assertEquals((byte) offs, memory.getByte(offs));
        }

        try {
            memory.getByte(-1);
            fail();
        } catch (IllegalMemoryAccessException e) {
            // Do nothing
        }

        try {
            memory.getByte(32);
            fail();
        } catch (IllegalMemoryAccessException e) {
            // Do nothing
        }

        try {
            memory.setByte(-1, (byte) 0);
            fail();
        } catch (IllegalMemoryAccessException e) {
            // Do nothing
        }

        try {
            memory.setByte(32, (byte) 0);
            fail();
        } catch (IllegalMemoryAccessException e) {
            // Do nothing
        }
    }
}
