package org.alcibiade.emulation.core.memory;

import org.junit.Test;

public class MemoryUtilsTest {

    @Test
    public void testDumpBytes() {
        RandomAccessible memoryArea = new MemoryArea(512);
        MemoryUtils.dumpBytes(memoryArea, 0, 884);
        MemoryUtils.dump(memoryArea, 0, 884);
    }
}
