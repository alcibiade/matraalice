package org.alcibiade.emulation.core.memory;

import org.junit.Assert;
import org.junit.Test;

public class VoidMemoryTest {

    @Test
    public void testMemory() throws IllegalMemoryAccessException {
        VoidMemory memory = new VoidMemory();

        memory.setByte(14, (byte) 0x20);
        Assert.assertEquals((byte) 0x00, memory.getByte(14));

        memory.setDefaultValue(0xFD);
        Assert.assertEquals((byte) 0xFD, memory.getByte(14));
    }
}
