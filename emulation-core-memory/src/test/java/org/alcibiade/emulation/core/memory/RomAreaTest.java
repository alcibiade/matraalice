package org.alcibiade.emulation.core.memory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.zip.GZIPOutputStream;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.io.Resource;

public class RomAreaTest {

    @Test
    public void testBasicRomArea() throws IOException, IllegalMemoryAccessException {
        Resource imageResource = new TestRomImageResource("rom.bin", false);
        RandomAccessible rom = new RomArea(imageResource);
        Assert.assertEquals(rom.getByte(0), 1);
        Assert.assertEquals(rom.getByte(1), 2);
        Assert.assertEquals(rom.getByte(2), 3);
        Assert.assertEquals(rom.getByte(3), 4);

        try {
            rom.getByte(5);
            Assert.fail();
        } catch (IllegalMemoryAccessException ex) {
            // This should happen
        }

        try {
            rom.setByte(1, (byte) 0x20);
            Assert.fail();
        } catch (IllegalMemoryAccessException ex) {
            // This should happen
        }
    }

    @Test
    public void testCompressedRomArea() throws IOException, IllegalMemoryAccessException {
        Resource imageResource = new TestRomImageResource("rom.bin.gz", true);
        RandomAccessible rom = new RomArea(imageResource);
        Assert.assertEquals(rom.getByte(0), 1);
        Assert.assertEquals(rom.getByte(1), 2);
        Assert.assertEquals(rom.getByte(2), 3);
        Assert.assertEquals(rom.getByte(3), 4);

        try {
            rom.getByte(5);
            Assert.fail();
        } catch (IllegalMemoryAccessException ex) {
            // This should happen
        }
    }

    private class TestRomImageResource implements Resource {

        private String fileName;
        private boolean compressed;

        public TestRomImageResource(String fileName, boolean compressed) {
            this.fileName = fileName;
            this.compressed = compressed;
        }

        @Override
        public String getFilename() {
            return fileName;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            byte[] bytes = {1, 2, 3, 4};

            if (compressed) {
                ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
                GZIPOutputStream gzipOut = new GZIPOutputStream(bytesOut);
                gzipOut.write(bytes);
                gzipOut.close();

                bytes = bytesOut.toByteArray();
            }

            return new ByteArrayInputStream(bytes);
        }

        @Override
        public String getDescription() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean exists() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean isReadable() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean isOpen() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public URL getURL() throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public URI getURI() throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public File getFile() throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public long contentLength() throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public long lastModified() throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Resource createRelative(String string) throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}
