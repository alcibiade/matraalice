package org.alcibiade.emulation.core.memory;

import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;

public class MemoryMapperTest {

    @Test
    public void testReadWrite() throws IllegalMemoryAccessException {
        MemoryArea memoryA = new MemoryArea(32);
        MemoryArea memoryB = new MemoryArea(32);
        Map<Integer, RandomAccessible> areas = new HashMap<Integer, RandomAccessible>();
        areas.put(0, memoryA);
        areas.put(128, memoryB);
        areas.put(160, memoryB);
        MemoryMapper memory = new MemoryMapper(areas);

        for (int offs = 0; offs < 32; offs++) {
            memory.setByte(offs, (byte) offs);
            memory.setByte(offs + 128, (byte) offs);
        }

        for (int offs = 0; offs < 32; offs++) {
            assertEquals((byte) offs, memory.getByte(offs));
            assertEquals((byte) offs, memory.getByte(offs + 128));
            assertEquals((byte) offs, memory.getByte(offs + 160));
        }

        try {
            memory.getByte(-1);
            fail();
        } catch (IllegalMemoryAccessException e) {
            // Do nothing
        }

        try {
            memory.getByte(32);
            fail();
        } catch (IllegalMemoryAccessException e) {
            // Do nothing
        }

        try {
            memory.setByte(-1, (byte) 0);
            fail();
        } catch (IllegalMemoryAccessException e) {
            // Do nothing
        }

        try {
            memory.setByte(32, (byte) 0);
            fail();
        } catch (IllegalMemoryAccessException e) {
            // Do nothing
        }
    }
}
