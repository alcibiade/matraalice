package org.alcibiade.emulation.core.memory;

import org.junit.Assert;
import org.junit.Test;

public class MemoryCellTest {

    @Test
    public void testCell() throws IllegalMemoryAccessException {
        MemoryCell cell = new MemoryCell();
        cell.setByte(0, (byte) 0x20);
        Assert.assertEquals(0x20, cell.getByte(0));

        try {
            cell.setByte(1, (byte) 0x20);
            Assert.fail();
        } catch (IllegalMemoryAccessException ex) {
            // This should happen
        }
    }

    @Test
    public void testCellNotification() throws IllegalMemoryAccessException {
        ListenerStub listener = new ListenerStub();
        MemoryCell cell = new MemoryCell();

        cell.addMemoryCellListener(listener);
        cell.setByte(0, (byte) 0x41);
        Assert.assertEquals(0x41, listener.getLatestValue());

        cell.removeMemoryCellListener(listener);
        cell.setByte(0, (byte) 0x42);
        Assert.assertEquals(0x41, listener.getLatestValue());
    }

    private class ListenerStub implements MemoryCellListener {

        private int latestValue;

        @Override
        public void notifyMemoryCellUpdated(byte newValue) throws IllegalMemoryAccessException {
            this.latestValue = newValue;
        }

        public int getLatestValue() {
            return latestValue;
        }
    }
}
