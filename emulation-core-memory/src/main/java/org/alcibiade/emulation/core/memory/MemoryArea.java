package org.alcibiade.emulation.core.memory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemoryArea implements RandomAccessible {

    private Logger logger = LoggerFactory.getLogger(MemoryArea.class);
    private byte[] bytes;

    public MemoryArea(int capacity) {
        bytes = new byte[capacity];
    }

    protected MemoryArea() {
        // Do nothing
    }

    protected void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    @Override
    public byte getByte(int offset) throws IllegalMemoryAccessException {
        validateOffset(offset);
        byte result = bytes[offset];
        return result;
    }

    @Override
    public void setByte(int offset, byte newValue) throws IllegalMemoryAccessException {
        validateOffset(offset);
        bytes[offset] = newValue;
    }

    private void validateOffset(int offset) throws IllegalMemoryAccessException {
        if (offset < 0 || offset >= bytes.length) {
            throw new IllegalMemoryAccessException(offset);
        }
    }
}
