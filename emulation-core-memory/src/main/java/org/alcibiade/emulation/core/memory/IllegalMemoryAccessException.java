package org.alcibiade.emulation.core.memory;

import org.alcibiade.emulation.core.EmulationRuntimeException;

public class IllegalMemoryAccessException extends EmulationRuntimeException {

    public IllegalMemoryAccessException(int offset) {
        super(String.format("Illegal memory access at offset %dd/%Xh", offset, offset));
    }
}
