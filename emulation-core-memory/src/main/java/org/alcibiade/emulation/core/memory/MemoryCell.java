package org.alcibiade.emulation.core.memory;

import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemoryCell implements RandomAccessible {

    private Logger logger = LoggerFactory.getLogger(MemoryCell.class);
    private byte value;
    private Set<MemoryCellListener> listeners = new HashSet<MemoryCellListener>();

    @Override
    public byte getByte(int offset) throws IllegalMemoryAccessException {
        validateOffset(offset);
        return value;
    }

    @Override
    public void setByte(int offset, byte newValue) throws IllegalMemoryAccessException {
        validateOffset(offset);
        logger.debug(String.format("[MEM@%04X] %02X -> %02X", offset, this.value, newValue));
        this.value = newValue;

        for (MemoryCellListener listener : listeners) {
            listener.notifyMemoryCellUpdated(newValue);
        }
    }

    private void validateOffset(int offset) throws IllegalMemoryAccessException {
        if (offset < 0 || offset >= 1) {
            throw new IllegalMemoryAccessException(offset);
        }
    }

    public void addMemoryCellListener(MemoryCellListener listener) {
        listeners.add(listener);
    }

    public void removeMemoryCellListener(MemoryCellListener listener) {
        listeners.remove(listener);
    }
}
