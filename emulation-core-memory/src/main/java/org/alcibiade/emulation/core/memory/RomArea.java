package org.alcibiade.emulation.core.memory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;

public class RomArea extends MemoryArea {

    public static final String GZIP_SUFFIX = ".gz";
    private Logger logger = LoggerFactory.getLogger(RomArea.class);

    public RomArea(Resource imageFile) throws IOException {
        InputStream imageInputStream = imageFile.getInputStream();

        if (StringUtils.endsWithIgnoreCase(imageFile.getFilename(), GZIP_SUFFIX)) {
            imageInputStream = new GZIPInputStream(imageInputStream);
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        IOUtils.copy(imageInputStream, baos);
        byte[] bytes = baos.toByteArray();

        setBytes(bytes);
        logger.info(String.format("Loaded %d bytes from %s", bytes.length, imageFile));
        imageInputStream.close();
    }

    @Override
    public void setByte(int offset, byte value) throws IllegalMemoryAccessException {
        throw new IllegalMemoryAccessException(offset);
    }
}
