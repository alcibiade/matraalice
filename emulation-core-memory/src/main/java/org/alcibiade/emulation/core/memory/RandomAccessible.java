package org.alcibiade.emulation.core.memory;

public interface RandomAccessible {

    byte getByte(int offset) throws IllegalMemoryAccessException;

    void setByte(int offset, byte value) throws IllegalMemoryAccessException;
}
