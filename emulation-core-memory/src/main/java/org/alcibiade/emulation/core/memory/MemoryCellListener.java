package org.alcibiade.emulation.core.memory;

public interface MemoryCellListener {

    void notifyMemoryCellUpdated(byte newValue) throws IllegalMemoryAccessException;
}
