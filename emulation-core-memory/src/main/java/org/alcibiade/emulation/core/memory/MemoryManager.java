package org.alcibiade.emulation.core.memory;

public interface MemoryManager {

    byte readByte(int offset) throws IllegalMemoryAccessException;

    void writeByte(int offset, byte value) throws IllegalMemoryAccessException;

    short readWord(int offset) throws IllegalMemoryAccessException;

    void writeWord(int offset, short value) throws IllegalMemoryAccessException;
}
