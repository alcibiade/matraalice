package org.alcibiade.emulation.core.memory;

public class BigEndianMemoryManager implements MemoryManager {

    private RandomAccessible memory;

    public BigEndianMemoryManager(RandomAccessible memory) {
        this.memory = memory;
    }

    @Override
    public byte readByte(int offset) throws IllegalMemoryAccessException {
        return memory.getByte(offset);
    }

    @Override
    public void writeByte(int offset, byte value) throws IllegalMemoryAccessException {
        memory.setByte(offset, value);
    }

    @Override
    public short readWord(int offset) throws IllegalMemoryAccessException {
        int val1 = readByte(offset) << 8;
        byte val2 = readByte(offset + 1);
        short result = (short) ((val1 & 0xFF00) | (val2 & 0x00FF));
        return result;
    }

    @Override
    public void writeWord(int offset, short value) throws IllegalMemoryAccessException {
        writeByte(offset, (byte) ((value & 0xFF00) >> 8));
        writeByte(offset + 1, (byte) (value & 0x00FF));
    }
}
