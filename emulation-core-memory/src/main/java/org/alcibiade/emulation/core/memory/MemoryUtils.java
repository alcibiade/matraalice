package org.alcibiade.emulation.core.memory;

public class MemoryUtils {

    private static final int LINE_BYTES = 16;

    public static String dumpBytes(RandomAccessible memory, int offset, int size) {
        StringBuilder text = new StringBuilder();

        for (int i = 0; i < size; i++) {
            String hex;
            try {
                hex = String.format("%02X", 0xFF & memory.getByte(offset + i));
            } catch (IllegalMemoryAccessException e) {
                hex = "..";
            }

            text.append(hex);
            text.append(' ');
        }

        return text.toString();
    }

    public static String dump(RandomAccessible memory, int offset, int size) {
        StringBuilder text = new StringBuilder();

        int remaining = size;
        int currentOffset = offset;

        while (remaining > 0) {
            String line = dumpLine(memory, currentOffset, remaining);
            text.append(line);
            text.append('\n');
            remaining -= LINE_BYTES;
            currentOffset += LINE_BYTES;
        }

        return text.toString();
    }

    private static String dumpLine(RandomAccessible memory, int currentOffset, int remaining) {
        StringBuilder line = new StringBuilder();
        line.append(String.format("%08X: ", currentOffset));

        int lineLength = Math.min(remaining, LINE_BYTES);

        for (int lineOffs = 0; lineOffs < LINE_BYTES; lineOffs++) {
            String hex = "  ";

            if (lineOffs < lineLength) {
                try {
                    int offset = currentOffset + lineOffs;
                    int value = memory.getByte(offset) & 0xFF;
                    hex = String.format("%02X", value);

                } catch (IllegalMemoryAccessException e) {
                    hex = "  ";
                }
            }

            line.append(hex);

            if (lineOffs % 2 == 1) {
                line.append(' ');
            }
        }

        line.append(" | ");

        for (int lineOffs = 0; lineOffs < lineLength; lineOffs++) {
            try {
                int offset = currentOffset + lineOffs;
                int value = memory.getByte(offset);

                if (value >= 0x21 && value < 0x7E) {
                    line.append(String.format("%c", value));
                } else {
                    line.append('.');
                }
            } catch (IllegalMemoryAccessException e) {
                line.append(' ');
            }

        }

        return line.toString();
    }
}
