package org.alcibiade.emulation.core.memory;

public class VoidMemory implements RandomAccessible {

    private byte defaultValue = 0x00;

    public void setDefaultValue(int defaultValue) {
        this.defaultValue = (byte)defaultValue;
    }

    @Override
    public byte getByte(int offset) throws IllegalMemoryAccessException {
        return defaultValue;
    }

    @Override
    public void setByte(int offset, byte value) throws IllegalMemoryAccessException {
        // Do nothing
    }
}
