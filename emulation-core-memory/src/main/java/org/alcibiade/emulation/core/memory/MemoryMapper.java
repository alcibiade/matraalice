package org.alcibiade.emulation.core.memory;

import java.util.Map;

public class MemoryMapper implements RandomAccessible {

    private Map<Integer, RandomAccessible> memoryAreas;

    public MemoryMapper(Map<Integer, RandomAccessible> memoryAreas) {
        assert !memoryAreas.isEmpty();
        this.memoryAreas = memoryAreas;
    }

    @Override
    public byte getByte(int offset) throws IllegalMemoryAccessException {
        int nearestOffset = findNearestOffset(offset);
        RandomAccessible memoryArea = memoryAreas.get(nearestOffset);
        return memoryArea.getByte(offset - nearestOffset);
    }

    @Override
    public void setByte(int offset, byte value) throws IllegalMemoryAccessException {
        int nearestOffset = findNearestOffset(offset);
        RandomAccessible memoryArea = memoryAreas.get(nearestOffset);
        memoryArea.setByte(offset - nearestOffset, value);
    }

    private int findNearestOffset(int offset) throws IllegalMemoryAccessException {
        int nearestOffset = -1;

        for (int o : memoryAreas.keySet()) {
            if (o > nearestOffset && o <= offset) {
                nearestOffset = o;
            }
        }

        if (nearestOffset < 0) {
            throw new IllegalMemoryAccessException(offset);
        }

        return nearestOffset;
    }
}
