package org.alcibiade.emulation.core;

/**
 * This is the parent of actual concrete exception raised by the emulated platform.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
public abstract class EmulationRuntimeException extends Exception {

    public EmulationRuntimeException(String message) {
        super(message);
    }

    public EmulationRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
