package org.alcibiade.emulation.core;

public interface Chip {

    String getDebugInformations();
}
