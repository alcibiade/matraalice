package org.alcibiade.emulation.core.ui.swing;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.annotation.PostConstruct;
import javax.swing.JFrame;
import org.alcibiade.emulation.core.keyboard.KeyboardController;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.video.Gpu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

public class EmulatorFrame implements KeyListener {

    private Logger logger = LoggerFactory.getLogger(EmulatorFrame.class);
    private String title;
    private JFrame mainFrame;
    private Gpu gpu;
    private KeyboardController keyboardController;

    public EmulatorFrame() {
    }

    @Required
    public void setGpu(Gpu gpu) {
        this.gpu = gpu;
    }

    @Required
    public void setKeyboardController(KeyboardController keyboardController) {
        this.keyboardController = keyboardController;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @PostConstruct
    public void init() {
        this.mainFrame = initializeFrame();
        this.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension mysize = this.mainFrame.getSize();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.mainFrame.setLocation(screenSize.width / 2 - (mysize.width / 2),
                screenSize.height / 2 - (mysize.height / 2));
        this.mainFrame.addKeyListener(this);

        this.mainFrame.setVisible(true);
    }

    private JFrame initializeFrame() {
        JFrame frame = new JFrame(title);
        Container contentPane = frame.getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(BorderLayout.CENTER, new EmulationVideoCanvas(this.gpu));
        frame.pack();
        return frame;
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        // Do nothing
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        logger.debug("Key pressed: " + ke);
        try {
            keyboardController.processKeyPress(ke.getKeyCode());
        } catch (IllegalMemoryAccessException ex) {
            logger.warn(ex.getLocalizedMessage(), ex);
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        logger.debug("Key released: " + ke);
        try {
            keyboardController.processKeyRelease(ke.getKeyCode());
        } catch (IllegalMemoryAccessException ex) {
            logger.warn(ex.getLocalizedMessage(), ex);
        }
    }
}
