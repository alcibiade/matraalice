package org.alcibiade.emulation.core.video;

import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.springframework.beans.factory.annotation.Required;

public abstract class AbstractGpu implements Gpu {

    private RandomAccessible memory;

    @Required
    public void setMemory(RandomAccessible memory) {
        this.memory = memory;
    }

    protected RandomAccessible getMemory() {
        return memory;
    }
}
