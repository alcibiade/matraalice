package org.alcibiade.emulation.core.keyboard;

import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public interface KeyboardController {

    void processKeyPress(int keyCode) throws IllegalMemoryAccessException;

    void processKeyRelease(int keyCode) throws IllegalMemoryAccessException;
}
