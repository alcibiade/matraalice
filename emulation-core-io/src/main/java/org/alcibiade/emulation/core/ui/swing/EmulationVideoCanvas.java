package org.alcibiade.emulation.core.ui.swing;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JComponent;
import org.alcibiade.emulation.core.video.Gpu;

public class EmulationVideoCanvas extends JComponent {

    private static final long serialVersionUID = 1L;
    private Gpu gpu;

    public EmulationVideoCanvas(Gpu gpu) {
        this.gpu = gpu;
        Timer refreshTimer = new Timer();

        refreshTimer.schedule(new RefreshTask(), 500, 500);
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension gpuSize = gpu.getDisplayResolution();
        return new Dimension(2 * gpuSize.width, 2 * gpuSize.height);
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Dimension displaySize = getSize();
        Dimension gpuSize = gpu.getDisplayResolution();
        g2.scale(displaySize.getWidth() / gpuSize.getWidth(),
                displaySize.getHeight() / gpuSize.getHeight());
        gpu.paint(g2);
    }

    private class RefreshTask extends TimerTask {

        @Override
        public void run() {
            repaint();
        }
    }
}
