package org.alcibiade.emulation.core.video;

import java.awt.Dimension;
import java.awt.Graphics2D;
import org.alcibiade.emulation.core.Chip;

public interface Gpu extends Chip {

    Dimension getDisplayResolution();

    void paint(Graphics2D graphics2D);
}
