package org.alcibiade.emulation.core.video;

public interface VdgFont {

    byte[] getCharacterShape(int character);
}
