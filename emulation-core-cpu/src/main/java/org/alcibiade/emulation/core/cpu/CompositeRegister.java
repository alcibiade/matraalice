package org.alcibiade.emulation.core.cpu;

public class CompositeRegister extends AbstractRegister {

    private Register l;
    private Register h;

    public CompositeRegister(String name, Register high, Register low) {
        super(name);
        this.l = low;
        this.h = high;
    }

    public Register getLowBits() {
        return l;
    }

    public Register getHighBits() {
        return h;
    }

    @Override
    public int getSignedValue() {
        return BinUtils.convertToSigned(getRawValue(), 8 * getSize());
    }

    @Override
    public int getUnsignedValue() {
        return BinUtils.convertToUnsigned(getRawValue(), 8 * getSize());
    }

    private int getRawValue() {
        return (h.getSignedValue() << 8) | l.getSignedValue() & 0x00FF;
    }

    @Override
    public void setValue(int value) {
        byte highValue = (byte) ((value & 0xFF00) >> 8);
        byte lowValue = (byte) (value & 0x00FF);
        l.setValue(lowValue);
        h.setValue(highValue);
    }

    @Override
    public String toString() {
        return h.toString() + "|" + l.toString();
    }

    @Override
    public int getSize() {
        return l.getSize() + h.getSize();
    }
}
