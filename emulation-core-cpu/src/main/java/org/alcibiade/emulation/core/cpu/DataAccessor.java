package org.alcibiade.emulation.core.cpu;

import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public interface DataAccessor {

    int getSignedValue() throws IllegalMemoryAccessException;

    int getUnsignedValue() throws IllegalMemoryAccessException;

    void setValue(int value) throws IllegalMemoryAccessException;
    
    String getName();
}
