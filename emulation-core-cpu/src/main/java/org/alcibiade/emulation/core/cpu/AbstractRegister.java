package org.alcibiade.emulation.core.cpu;

public abstract class AbstractRegister implements Register {

    private String name;

    public AbstractRegister(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
