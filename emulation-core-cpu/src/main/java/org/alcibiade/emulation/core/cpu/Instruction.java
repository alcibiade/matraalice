package org.alcibiade.emulation.core.cpu;

import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public interface Instruction {

    int getSize();

    int getCycles();

    String getAssembly();

    String getNotes();

    void execute() throws IllegalMemoryAccessException;
}
