package org.alcibiade.emulation.core.cpu;

import org.alcibiade.emulation.core.Chip;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public interface Cpu extends Chip {

    int runCycle() throws IllegalMemoryAccessException, IllegalInstructionCodeException;
}
