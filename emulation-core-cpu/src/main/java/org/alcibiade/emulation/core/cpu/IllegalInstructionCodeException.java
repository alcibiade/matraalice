package org.alcibiade.emulation.core.cpu;

import org.alcibiade.emulation.core.EmulationRuntimeException;

public class IllegalInstructionCodeException extends EmulationRuntimeException {

    public IllegalInstructionCodeException(int offset, int opcode) {
        super(String.format("Illegal instruction %02Xh at offset %dd/%Xh", opcode, offset, offset));
    }
}
