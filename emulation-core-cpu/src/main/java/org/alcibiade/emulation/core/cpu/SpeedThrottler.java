package org.alcibiade.emulation.core.cpu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpeedThrottler {

    private static final long SLEEP_QUANTA_MILLIS = 10;
    private static final int TIME_CHECK_INTERVAL = 100;
    private long startTime;
    private long cyclesComputed;
    private long frequency;
    private int adjustmentsSinceLastCheck;
    private Logger logger = LoggerFactory.getLogger(SpeedThrottler.class);

    public SpeedThrottler(long frequency) {
        this.frequency = frequency;
        this.cyclesComputed = 0;
        this.startTime = getCurrentTimeMillis();
        this.adjustmentsSinceLastCheck = 0;
    }

    public void adjustTime(long cycles) {
        cyclesComputed += cycles;
        adjustmentsSinceLastCheck += 1;

        if (adjustmentsSinceLastCheck >= TIME_CHECK_INTERVAL) {
            throttle();
            adjustmentsSinceLastCheck = 0;
        }
    }

    private void throttle() {
        long nativeDuration = cyclesComputed * 1000 / frequency;
        long actualDuration = getEllapsedTimeMillis();

        if (actualDuration + SLEEP_QUANTA_MILLIS < nativeDuration) {
            logger.debug("Sleeping since elapsed time is " + (nativeDuration - actualDuration)
                    + "ms ahead");
            try {
                Thread.sleep(SLEEP_QUANTA_MILLIS);
            } catch (InterruptedException e) {
                // Do nothing
            }
        }
    }

    public long getEllapsedTimeMillis() {
        return 1 + getCurrentTimeMillis() - startTime;
    }

    private long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }
}
