package org.alcibiade.emulation.core.cpu;

import java.util.HashSet;
import java.util.Set;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.MemoryUtils;
import org.alcibiade.emulation.core.memory.RandomAccessible;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

public abstract class AbstractCpu implements Cpu {

    private Logger logger = LoggerFactory.getLogger(AbstractCpu.class);
    private RandomAccessible memory;
    private Set<Integer> maskedOperations = new HashSet<Integer>();

    public AbstractCpu() {
        maskedOperations.add(0xF861);
        maskedOperations.add(0xF862);
    }

    @Required
    public void setMemory(RandomAccessible memory) {
        this.memory = memory;
    }

    protected RandomAccessible getMemory() {
        return memory;
    }

    @Override
    public int runCycle() throws IllegalMemoryAccessException, IllegalInstructionCodeException {
        int pc = getProgramAddress();
        Instruction instruction = parseInstructionAt(pc);

        if (logger.isDebugEnabled()) {
            if (maskedOperations.contains(pc)) {
                logger.debug("..");
            } else {
                if (pc < 512 || (pc >= 0xE320 && pc < 0xE380)) {
                    logger.debug("CPU Status:\n" + getDebugInformations());
                }

                logger.debug(String.format("%04X: %-10s  %-20s ; %s", pc,
                        MemoryUtils.dumpBytes(memory, pc, instruction.getSize()),
                        instruction.getAssembly(), instruction.getNotes()));
            }
        }

        instruction.execute();
        return instruction.getCycles();
    }

    protected abstract int getProgramAddress() throws IllegalMemoryAccessException;

    protected abstract Instruction parseInstructionAt(int offset)
            throws IllegalInstructionCodeException, IllegalMemoryAccessException;
}
