package org.alcibiade.emulation.core.cpu;

public interface Register extends DataAccessor {

    @Override
    int getSignedValue();

    @Override
    int getUnsignedValue();

    @Override
    void setValue(int value);

    @Override
    String getName();

    int getSize();
}
