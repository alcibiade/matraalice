package org.alcibiade.emulation.core.cpu;

public class StaticRegister extends AbstractRegister {

    private int bits;
    private int value;

    public StaticRegister(String name, int bits) {
        super(name);
        assert bits > 0;
        assert bits < 32;
        assert bits % 8 == 0;
        this.bits = bits;
        this.value = 0;
    }

    public byte getValueAsByte() {
        return (byte) value;
    }

    @Override
    public int getSignedValue() {
        return BinUtils.convertToSigned(value, bits);
    }

    @Override
    public int getUnsignedValue() {
        return BinUtils.convertToUnsigned(value, bits);
    }

    @Override
    public void setValue(int value) {
        this.value = value & computeBitMask();
    }

    private int computeBitMask() {
        int mask = 0;

        for (int i = 0; i < bits; i++) {
            mask = (mask << 1) | 1;
        }

        return mask;
    }

    @Override
    public int getSize() {
        return bits / 8;
    }

    @Override
    public String toString() {
        return String.format("%08Xh", value);
    }
}
