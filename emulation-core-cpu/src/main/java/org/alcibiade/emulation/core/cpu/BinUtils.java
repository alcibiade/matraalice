package org.alcibiade.emulation.core.cpu;

public abstract class BinUtils {

    public static int convertToUnsigned(int value, int bits) {
        int result = value;
        int maxValue = 1 << bits;

        while (result < 0) {
            result += maxValue;
        }

        return result % maxValue;
    }

    public static int convertToSigned(int value, int bits) {
        int result = value;
        int maxValue = 1 << bits;

        while (result < maxValue / 2) {
            result += maxValue;
        }

        while (result >= maxValue / 2) {
            result -= maxValue;
        }

        return result;
    }
}
