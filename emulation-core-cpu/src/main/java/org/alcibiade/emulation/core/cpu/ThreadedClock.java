package org.alcibiade.emulation.core.cpu;

import javax.annotation.PostConstruct;
import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

public class ThreadedClock extends Thread implements CpuClock {

    private Logger logger = LoggerFactory.getLogger(ThreadedClock.class);
    private long frequency;
    private Cpu cpu;
    private boolean interrupted;

    public ThreadedClock() {
        super.setDaemon(false);
        super.setName("CpuClock-" + super.getId());
    }

    @Required
    public void setFrequency(long frequency) {
        this.frequency = frequency;
    }

    @Required
    public void setCpu(Cpu cpu) {
        this.cpu = cpu;
    }

    @PostConstruct
    public void init() {
        interrupted = false;
        start();
    }

    @Override
    public void interrupt() {
        this.interrupted = true;
    }

    @Override
    public void run() {
        logger.debug("Starting clock thread execution at frequency of " + frequency);
        SpeedThrottler throttler = new SpeedThrottler(frequency);

        try {
            while (!interrupted) {
                int cycles = cpu.runCycle();
                throttler.adjustTime(cycles);
            }
        } catch (IllegalMemoryAccessException ex) {
            logger.error(ex.getLocalizedMessage(), ex);
        } catch (IllegalInstructionCodeException ex) {
            logger.error(ex.getLocalizedMessage(), ex);
        } finally {
            logger.debug("Processor debug informations:\n" + cpu.getDebugInformations());
        }
    }
}
