package org.alcibiade.emulation.core.cpu;

import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.VoidMemory;
import org.junit.Assert;
import org.junit.Test;

public class AbstractCpuTest {

    @Test
    public void testCpu() throws IllegalMemoryAccessException, IllegalInstructionCodeException {
        CpuStub cpu = new CpuStub();
        cpu.setMemory(new VoidMemory());
        cpu.runCycle();
        Assert.assertEquals(1, cpu.getProgramAddress());
    }
}
