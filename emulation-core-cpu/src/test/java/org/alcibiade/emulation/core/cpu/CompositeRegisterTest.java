package org.alcibiade.emulation.core.cpu;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CompositeRegisterTest {

    @Test
    public void testRegisterValue() {
        StaticRegister al = new StaticRegister("AL", 8);
        StaticRegister ah = new StaticRegister("AH", 8);
        CompositeRegister ax = new CompositeRegister("AX", ah, al);

        assertEquals(al, ax.getLowBits());
        assertEquals(ah, ax.getHighBits());
        assertEquals(2, ax.getSize());
        ax.toString();

        ax.setValue((short) 0x4DF3);
        assertEquals((byte) 0x4D, ah.getValueAsByte());
        assertEquals((byte) 0xF3, al.getValueAsByte());
        assertEquals((short) 0x4DF3, ax.getSignedValue());

        al.setValue((byte) 0xA5);
        assertEquals((byte) 0x4D, ah.getValueAsByte());
        assertEquals((byte) 0xA5, al.getValueAsByte());
        assertEquals((short) 0x4DA5, ax.getSignedValue());

        ah.setValue((byte) 0x3D);
        assertEquals((byte) 0x3D, ah.getValueAsByte());
        assertEquals((byte) 0xA5, al.getValueAsByte());
        assertEquals((short) 0x3DA5, ax.getSignedValue());
    }
}
