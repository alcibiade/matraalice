package org.alcibiade.emulation.core.cpu;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class BinUtilsTest {

    @Test
    public void testConversion() {
        assertEquals((short) 0xABCD, BinUtils.convertToSigned(0xABCD, 16));
        assertEquals((byte) 0xABCD, BinUtils.convertToSigned(0xABCD, 8));

        assertEquals(-64, BinUtils.convertToSigned(-40000, 8));
        assertEquals(192, BinUtils.convertToUnsigned(-40000, 8));
        assertEquals(25536, BinUtils.convertToSigned(-40000, 16));
        assertEquals(25536, BinUtils.convertToUnsigned(-40000, 16));

        assertEquals(-10, BinUtils.convertToSigned(-10, 8));
        assertEquals(246, BinUtils.convertToUnsigned(-10, 8));
        assertEquals(-10, BinUtils.convertToSigned(-10, 16));
        assertEquals(65526, BinUtils.convertToUnsigned(-10, 16));

        assertEquals(0, BinUtils.convertToSigned(0, 8));
        assertEquals(0, BinUtils.convertToUnsigned(0, 8));
        assertEquals(0, BinUtils.convertToSigned(0, 16));
        assertEquals(0, BinUtils.convertToUnsigned(0, 16));

        assertEquals(10, BinUtils.convertToSigned(10, 8));
        assertEquals(10, BinUtils.convertToUnsigned(10, 8));
        assertEquals(10, BinUtils.convertToSigned(10, 16));
        assertEquals(10, BinUtils.convertToUnsigned(10, 16));

        assertEquals(-56, BinUtils.convertToSigned(200, 8));
        assertEquals(200, BinUtils.convertToUnsigned(200, 8));
        assertEquals(200, BinUtils.convertToSigned(200, 16));
        assertEquals(200, BinUtils.convertToUnsigned(200, 16));

        assertEquals(44, BinUtils.convertToSigned(300, 8));
        assertEquals(44, BinUtils.convertToUnsigned(300, 8));
        assertEquals(300, BinUtils.convertToSigned(300, 16));
        assertEquals(300, BinUtils.convertToUnsigned(300, 16));

        assertEquals(64, BinUtils.convertToSigned(40000, 8));
        assertEquals(64, BinUtils.convertToUnsigned(40000, 8));
        assertEquals(-25536, BinUtils.convertToSigned(40000, 16));
        assertEquals(40000, BinUtils.convertToUnsigned(40000, 16));
    }
}
