package org.alcibiade.emulation.core.cpu;

import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;

public class CpuStub extends AbstractCpu {

    private int pc = 0;

    @Override
    protected int getProgramAddress() throws IllegalMemoryAccessException {
        return pc;
    }

    @Override
    protected Instruction parseInstructionAt(int offset) throws IllegalInstructionCodeException,
            IllegalMemoryAccessException {
        return new Instruction() {

            @Override
            public int getSize() {
                return 1;
            }

            @Override
            public int getCycles() {
                return 4;
            }

            @Override
            public String getAssembly() {
                return "NOP";
            }

            @Override
            public String getNotes() {
                return "Dummy stub op";
            }

            @Override
            public void execute() throws IllegalMemoryAccessException {
                pc += getSize();
            }
        };
    }

    @Override
    public String getDebugInformations() {
        return String.format("[CPU Stub] Current PC is %04X", pc);
    }
}
