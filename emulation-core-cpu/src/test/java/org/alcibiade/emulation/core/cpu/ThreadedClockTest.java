package org.alcibiade.emulation.core.cpu;

import org.alcibiade.emulation.core.memory.IllegalMemoryAccessException;
import org.alcibiade.emulation.core.memory.VoidMemory;
import org.junit.Assert;
import org.junit.Test;

public class ThreadedClockTest {

    @Test
    public void testClock() throws InterruptedException, IllegalMemoryAccessException {
        CpuStub cpu = new CpuStub();
        cpu.setMemory(new VoidMemory());

        ThreadedClock clock = new ThreadedClock();
        clock.setFrequency(10000);
        clock.setCpu(cpu);
        clock.init();
        Thread.sleep(100);
        clock.interrupt();
        clock.join();

        Assert.assertTrue(cpu.getProgramAddress() > 0);
    }
}
