package org.alcibiade.emulation.core.cpu;

import org.junit.Assert;
import org.junit.Test;

public class SpeedThrottlerTest {

    @Test
    public void testThrottle() {
        // 10kHz Throttler
        SpeedThrottler throttler = new SpeedThrottler(10000);

        long startTime = System.currentTimeMillis();

        for (int i = 0; i < 1000; i++) {
            throttler.adjustTime(1);
        }

        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        Assert.assertEquals(100, duration, 50);
    }
}
