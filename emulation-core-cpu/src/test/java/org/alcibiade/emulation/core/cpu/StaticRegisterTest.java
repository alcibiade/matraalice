package org.alcibiade.emulation.core.cpu;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class StaticRegisterTest {

    @Test
    public void testRegisterValue() {
        StaticRegister reg = new StaticRegister("TestReg", 8);
        reg.setValue((byte) 64);
        assertEquals((byte) 64, reg.getValueAsByte());
        assertEquals(64, reg.getSignedValue());
        assertEquals(64, reg.getUnsignedValue());

        reg.setValue((byte) -1);
        assertEquals((byte) -1, reg.getValueAsByte());
        assertEquals(-1, reg.getSignedValue());
        assertEquals(255, reg.getUnsignedValue());

        reg.setValue(256);
        assertEquals(0, reg.getValueAsByte());
        assertEquals(0, reg.getSignedValue());
        assertEquals(0, reg.getUnsignedValue());
    }
}
